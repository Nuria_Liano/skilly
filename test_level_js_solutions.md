# [SOLUCION] Nivelador Javascript

# Nivelador Javascript

## 1. Recoger el valor de un input

~~~js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recoger valor de input</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        form {
            background: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        input {
            padding: 10px;
            margin-right: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        button {
            padding: 10px 20px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
    </style>
</head>
<body>
    <form>
        <input type="text" id="userInput" placeholder="Introduce algo">
        <button type="button" onclick="showValue()">Mostrar valor</button>
    </form>

    <script>
        function showValue() {
            var input = document.getElementById('userInput').value;
            alert(input);
        }
    </script>
</body>
</html>

~~~

## 2. Cambiar el color de fondo

~~~js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cambiar color de fondo</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        button {
            padding: 10px 20px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
    </style>
</head>
<body>
    <button onclick="changeBackgroundColor()">Cambiar color de fondo</button>

    <script>
        function changeBackgroundColor() {
            var colors = ['#f0f0f0', '#ffcccc', '#ccffcc', '#ccccff', '#ffffcc'];
            var randomColor = colors[Math.floor(Math.random() * colors.length)];
            document.body.style.backgroundColor = randomColor;
        }
    </script>
</body>
</html>

~~~

## 3. Validar un formulario

~~~js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Validar formulario</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        form {
            background: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        input {
            display: block;
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        button {
            padding: 10px 20px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
        .error {
            color: red;
            font-size: 0.9em;
        }
    </style>
</head>
<body>
    <form onsubmit="return validateForm()">
        <input type="text" id="name" placeholder="Nombre">
        <input type="email" id="email" placeholder="Correo electrónico">
        <button type="submit">Enviar</button>
        <div id="errorMessage" class="error"></div>
    </form>

    <script>
        function validateForm() {
            var name = document.getElementById('name').value;
            var email = document.getElementById('email').value;
            var errorMessage = document.getElementById('errorMessage');
            errorMessage.textContent = '';

            if (name === '' || email === '') {
                errorMessage.textContent = 'Todos los campos son obligatorios.';
                return false;
            }

            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            if (!emailPattern.test(email)) {
                errorMessage.textContent = 'Por favor, introduce un correo electrónico válido.';
                return false;
            }

            alert('Formulario enviado con éxito');
            return true;
        }
    </script>
</body>
</html>

~~~

## 4. Crear un contador

~~~js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contador</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        button {
            padding: 10px 20px;
            margin: 5px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
        #counter {
            font-size: 2em;
            margin: 20px 0;
        }
    </style>
</head>
<body>
    <div id="counter">0</div>
    <button onclick="increaseCounter()">Aumentar</button>
    <button onclick="decreaseCounter()">Disminuir</button>

    <script>
        var counter = 0;

        function increaseCounter() {
            counter++;
            document.getElementById('counter').textContent = counter;
        }

        function decreaseCounter() {
            counter--;
            document.getElementById('counter').textContent = counter;
        }
    </script>
</body>
</html>

~~~

## 5. Mostrar/Ocultar contenido

~~~js
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mostrar/Ocultar contenido</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        button {
            padding: 10px 20px;
            margin: 5px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
        #text {
            display: none;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <button onclick="toggleText()">Mostrar/Ocultar texto</button>
    <p id="text">Este es un texto que puede ser mostrado u ocultado.</p>

    <script>
        function toggleText() {
            var text = document.getElementById('text');
            if (text.style.display === 'none' || text.style.display === '') {
                text.style.display = 'block';
            } else {
                text.style.display = 'none';
            }
        }
    </script>
</body>
</html>

~~~