# Presupuesto para ADMINISTRACIÓN DE REDES Y SISTEMAS OPERATIVOS | Carlos

## Detalles del Proyecto

- **Inicio del proyecto:** 29 de diciembre de 2024
- **Fecha límite de entrega:** 5 de enero de 2025
- **Duración estimada:** 2 horas diarias
- **Tarifa por hora:** 25 €  
- **Clase de revisión intermedia:** 1 hora  
- **Clase de revisión final:** 0.5 horas  

---

## Cronograma y Actividades

### **Día 1 - 29 de diciembre de 2024**
- Configuración inicial del entorno.
- Instalación de la máquina virtual.
- Instalación del sistema operativo Fedora Server 40.

### **Día 2 - 30 de diciembre de 2024**
- Configuración básica de usuarios y contraseñas.
- Evidencias de configuración inicial.

### **Día 3 - 31 de diciembre de 2024**
- Configuración del servidor MongoDB utilizando Docker Compose.
- Creación y pruebas de persistencia de datos.

### **Día 4 - 1 de enero de 2025**
- Configuración del servidor Python/Flask.
- Conexión entre el servidor MongoDB y el servidor Flask.

### **Día 5 - 2 de enero de 2025**
- Implementación de un reverse proxy utilizando Apache en Docker.
- Configuración de HTTP.

### **Día 6 - 3 de enero de 2025**
- Configuración de HTTPS en el reverse proxy.
- Generación de certificados SSL autofirmados.

### **Día 7 - 4 de enero de 2025**
- Configuración del cortafuegos para limitar accesos a puertos específicos.
- Pruebas finales de seguridad.

### **Día 8 - 5 de enero de 2025**
- **Clase de revisión final (0.5 horas):** Revisión completa del proyecto y entrega final.

---

## Clases de Explicación

- **Clase intermedia (1 hora):** A mitad del proyecto (2 de enero de 2025) para explicar los avances.
- **Clase final (0.5 horas):** Revisión del proyecto finalizado y entrega (5 de enero de 2025).

---

## Coste Total del Proyecto

| Concepto                          | Horas | Tarifa (€) | Total (€) |
|-----------------------------------|-------|------------|-----------|
| Desarrollo del proyecto           | 14    | 24         | 336      |
| Clase intermedia                  | 1     | 25         | 25        |
| Clase final                       | 0.5   | 25         | 12.5      |
| **Total**                         |       |            | **373.5** |

---

**Nota:** Los tiempos estimados y tareas están sujetos a cambios menores según la complejidad encontrada durante el desarrollo.
