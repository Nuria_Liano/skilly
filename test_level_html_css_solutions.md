# [SOLUCIÓN] Nivelador HTML y CSS

## 1. Estructura Básica de HTML

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página Básica</title>
</head>
<body>
    <header>
        <h1>Encabezado</h1>
    </header>
    <main>
        <section>
            <h2>Sección Principal</h2>
            <p>Este es el contenido principal de la página.</p>
        </section>
    </main>
    <footer>
        <p>Pie de página</p>
    </footer>
</body>
</html>
~~~

## 2. Estilizar un Formulario

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario de contacto</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <form>
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required>
        <label for="correo">Correo Electrónico:</label>
        <input type="email" id="correo" name="correo" required>
        <button type="submit">Enviar</button>
    </form>
</body>
</html>

~~~

~~~css
body {
    font-family: Arial, sans-serif;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;
    background-color: #f0f0f0;
}
form {
    background: #fff;
    padding: 20px;
    border-radius: 5px;
    box-shadow: 0 0 10px rgba(0,0,0,0.1);
    max-width: 400px;
    width: 100%;
}
label {
    display: block;
    margin-bottom: 5px;
}
input {
    display: block;
    width: 100%;
    padding: 10px;
    margin-bottom: 10px;
    border: 1px solid #ccc;
    border-radius: 3px;
}
button {
    padding: 10px 20px;
    background: #007bff;
    color: #fff;
    border: none;
    border-radius: 3px;
    cursor: pointer;
}
button[type="submit"]:hover {
    background: #0056b3;
}

~~~

## 3. Menú de Navegación

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú de Navegación</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <nav>
        <ul>
            <li><a href="#home">Inicio</a></li>
            <li><a href="#about">Acerca de</a></li>
            <li><a href="#contact">Contacto</a></li>
        </ul>
    </nav>
</body>
</html>
~~~

~~~css
nav ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
    display: flex;
    background-color: #333;
}
nav ul li {
    margin-right: 10px;
}
nav ul li a {
    display: block;
    padding: 10px 20px;
    color: white;
    text-decoration: none;
}
nav ul li a:hover {
    background-color: #0056b3;
}

~~~

## 4. Modelo de Caja (Box Model)

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modelo de Caja</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="caja">
        <p>Este es un contenido dentro de un contenedor estilizado usando el modelo de caja.</p>
    </div>
</body>
</html>


~~~

~~~css
.caja {
    width: 300px;
    padding: 20px;
    margin: 20px;
    border: 2px solid #333;
    background-color: #f9f9f9;
}
~~~

## 5. Diseño Responsivo

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Diseño Responsivo</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Encabezado</h1>
    </header>
    <main>
        <section>
            <h2>Sección Principal</h2>
            <p>Este es el contenido principal de la página.</p>
        </section>
    </main>
    <footer>
        <p>Pie de página</p>
    </footer>
</body>
</html>

~~~

~~~css
body {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: column;
    min-height: 100vh;
}
header, footer {
    background-color: #333;
    color: white;
    padding: 10px 20px;
    text-align: center;
}
main {
    flex: 1;
    padding: 20px;
}
section {
    background-color: #f0f0f0;
    padding: 20px;
    border-radius: 5px;
}
@media (max-width: 600px) {
    header, footer {
        text-align: left;
    }
    section {
        padding: 10px;
    }
}

~~~