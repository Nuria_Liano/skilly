document.addEventListener('DOMContentLoaded', inicio, false)

function inicio(){
    //recoger el nombre del formulario
    const name = document.getElementById("name"); 

    //cargar el nombre desde la cookie si existe
    const savedCookieName = getCookie("username");

    if (savedCookieName){
        name.value = savedCookieName;
    }

    //guardar el nombre de las cookies cuando se envia el formualario
    document.getElementById("commentForm").addEventListener("submit", () =>{
        setCookie("username", name.value, 7)
    })

    //crear la funcion de setcookie
    function setCookie(clave, valor, tiempo){
        //establecer la fecha de expiración
        const fechaExpiracion = new Date();

        //pasar el tiempo a milisegundo
        let milisegundos = fecha.getTime() + (tiempo *24 * 60 * 60 * 1000)

        fechaExpiracion.setTime(milisegundos); //tiempo en milisegundos

        //cambiar milisegundos texto
        let fechaExp =fechaExpiracion.toUTCString();

        document.cookie = clave + "=" + valor + ";" + "expires=" + fechaExp + ";" + "path=/";
    }

}