# Introducción al DOM en JavaScript

## ¿Qué es el DOM?

El DOM (Document Object Model) es una interfaz de programación para los documentos HTML y XML. Representa la estructura de un documento como un árbol de nodos, donde cada nodo es un objeto que representa una parte del documento, como un elemento, un atributo, o un texto. JavaScript puede interactuar con el DOM para manipular la estructura, el estilo y el contenido de la página en tiempo real.

## Cómo Funciona el DOM

El DOM permite que JavaScript acceda y manipule el contenido y la estructura de un documento HTML de las siguientes maneras:

1. **Acceso a Elementos del DOM:**
   - **`document.getElementById(id)`**: Selecciona un elemento por su ID.
   - **`document.querySelector(selector)`**: Selecciona el primer elemento que coincide con el selector CSS.
   - **`document.querySelectorAll(selector)`**: Selecciona todos los elementos que coinciden con el selector CSS.

2. **Manipulación de Contenidos:**
   - **`element.textContent`**: Cambia o devuelve el contenido textual de un elemento.
   - **`element.innerHTML`**: Cambia o devuelve el HTML interno de un elemento.
   - **`element.setAttribute(name, value)`**: Establece un atributo para el elemento.

3. **Manipulación de Estructura:**
   - **`element.appendChild(node)`**: Añade un nodo hijo al final de la lista de hijos de un elemento.
   - **`element.removeChild(node)`**: Elimina un nodo hijo de la lista de hijos de un elemento.
   - **`document.createElement(tagName)`**: Crea un nuevo elemento HTML.

4. **Manipulación de Estilos:**
   - **`element.style.propertyName`**: Cambia una propiedad CSS específica de un elemento.

5. **Eventos:**
   - **`element.addEventListener(event, function)`**: Adjunta un manejador de eventos a un elemento.
   - **Eventos comunes:** `click`, `mouseover`, `keydown`, `submit`, etc.

## Ejemplo Completo

Aquí tienes un ejemplo que ilustra cómo interactuar con el DOM para crear un pequeño formulario de contacto. Este ejemplo cubre la creación de elementos, la manipulación de contenido, la manipulación de estilos y la gestión de eventos.

### HTML

```html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario de Contacto</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        #formulario {
            width: 300px;
            margin: auto;
        }
        .input-group {
            margin-bottom: 15px;
        }
        .input-group label {
            display: block;
            margin-bottom: 5px;
        }
        .input-group input {
            width: 100%;
            padding: 8px;
            box-sizing: border-box;
        }
        #mensaje {
            margin-top: 20px;
            color: green;
        }
    </style>
</head>
<body>
    <div id="formulario">
        <h2>Contacto</h2>
        <div class="input-group">
            <label for="nombre">Nombre:</label>
            <input type="text" id="nombre">
        </div>
        <div class="input-group">
            <label for="email">Correo Electrónico:</label>
            <input type="email" id="email">
        </div>
        <button id="enviar">Enviar</button>
        <div id="mensaje"></div>
    </div>

    <script src="script.js"></script>
</body>
</html>
```

### JS

~~~js
// Obtener referencias a los elementos del DOM
const nombreInput = document.getElementById('nombre');
const emailInput = document.getElementById('email');
const enviarBtn = document.getElementById('enviar');
const mensajeDiv = document.getElementById('mensaje');

// Agregar un event listener al botón de enviar
enviarBtn.addEventListener('click', function() {
    // Obtener los valores de los campos de entrada
    const nombre = nombreInput.value;
    const email = emailInput.value;

    // Validar que los campos no estén vacíos
    if (nombre === '' || email === '') {
        mensajeDiv.textContent = 'Por favor, complete todos los campos.';
        mensajeDiv.style.color = 'red';
    } else {
        // Mostrar un mensaje de éxito
        mensajeDiv.textContent = `Gracias, ${nombre}. Nos pondremos en contacto contigo en ${email}.`;
        mensajeDiv.style.color = 'green';

        // Limpiar los campos
        nombreInput.value = '';
        emailInput.value = '';
    }
});
~~~

**Explicación del Ejemplo**

- Acceso a Elementos del DOM:
Usamos document.getElementById para obtener referencias a los elementos del formulario.

- Manipulación de Contenidos:
mensajeDiv.textContent se usa para mostrar mensajes de validación o éxito.

- Manipulación de Estilos:
mensajeDiv.style.color cambia el color del mensaje según el estado (error o éxito).

- Eventos:
enviarBtn.addEventListener('click', function) añade un manejador de eventos que se activa cuando el usuario hace clic en el botón de enviar. Se validan los campos y se muestra un mensaje en consecuencia.