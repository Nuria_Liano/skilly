# Bucles y condicionales

## 1. Suma de los primeros 100 números

Escribe un programa que calcule la suma de los números del 1 al 100 usando un bucle.

## 2. Cuenta regresiva

Escribe un programa que realice una cuenta regresiva desde 10 hasta 0 usando un bucle

## 3. Tabla de multiplicar

Escribe un programa que imprima la tabla de multiplicar del 5 usando un bucle

## 4. Contar vocales

Escribe un programa que cuente el número de vocales en una cadena de texto.

## 5. Número primo

Escribe un programa que verifique si un número dado es primo o no.

## 6. Invertir cadena

Escribe un programa que invierta una cadena de texto.

## 7. Suma de dígitos

Escribe un programa que sume todos los dígitos de un número entero positivo.

## 8. Número perfecto

Escribe un programa que determine si un número dado es un número perfecto.

> :black_joker: **PISTA**
>
>Un número perfecto es un número entero positivo que es igual a la suma de sus divisores propios (excluyendo el propio número). Por ejemplo, 6 es un número perfecto porque 1 + 2 + 3 = 6.