# Bucles y condicionales

## 1. Suma de los primeros 100 números

Escribe un programa que calcule la suma de los números del 1 al 100 usando un bucle.

~~~js
let suma = 0;

for (let i = 1; i <= 100; i++) {
    suma += i;
}

console.log(suma);

~~~

## 2. Cuenta regresiva

Escribe un programa que realice una cuenta regresiva desde 10 hasta 0 usando un bucle

~~~js
for (let i = 10; i >= 0; i--) {
    console.log(i);
}

~~~


## 3. Tabla de multiplicar

Escribe un programa que imprima la tabla de multiplicar del 5 usando un bucle

~~~js
const numero = 5;

for (let i = 1; i <= 10; i++) {
    console.log(numero + " x " + i + " = " + (numero * i));
}

~~~

## 4. Contar vocales

Escribe un programa que cuente el número de vocales en una cadena de texto.

~~~js
const texto = "Hola, cómo estás?";
let contadorVocales = 0;

for (let i = 0; i < texto.length; i++) {
    let letra = texto[i].toLowerCase();
    if (letra === 'a' || letra === 'e' || letra === 'i' || letra === 'o' || letra === 'u') {
        contadorVocales++;
    }
}

console.log(contadorVocales);

~~~


## 5. Número primo

Escribe un programa que verifique si un número dado es primo o no.

~~~js
const numero = 17;
let esPrimo = true;

for (let i = 2; i < numero; i++) {
    if (numero % i === 0) {
        esPrimo = false;
        break;
    }
}

if (esPrimo && numero > 1) {
    console.log(numero + " es un número primo.");
} else {
    console.log(numero + " no es un número primo.");
}

~~~


## 6. Invertir cadena

Escribe un programa que invierta una cadena de texto.

~~~js
const texto = "Hola mundo";
let textoInvertido = "";

for (let i = texto.length - 1; i >= 0; i--) {
    textoInvertido += texto[i];
}

console.log(textoInvertido);

~~~


## 7. Suma de dígitos

Escribe un programa que sume todos los dígitos de un número entero positivo.

~~~js
let numero = 12345;
let suma = 0;

while (numero > 0) {
    suma += numero % 10;
    numero = Math.floor(numero / 10);
}

console.log(suma);

~~~


## 8. Número perfecto

Escribe un programa que determine si un número dado es un número perfecto.

> :black_joker: **PISTA**
>
>Un número perfecto es un número entero positivo que es igual a la suma de sus divisores propios (excluyendo el propio número). Por ejemplo, 6 es un número perfecto porque 1 + 2 + 3 = 6.

~~~js
const numero = 28;
let sumaDivisores = 0;

for (let i = 1; i < numero; i++) {
    if (numero % i === 0) {
        sumaDivisores += i;
    }
}

if (sumaDivisores === numero) {
    console.log(numero + " es un número perfecto.");
} else {
    console.log(numero + " no es un número perfecto.");
}

~~~
