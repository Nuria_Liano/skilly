Configuración de Routers MikroTik con Comandos

1. Acceso al Router

Para configurar un router MikroTik, se puede acceder mediante:

WinBox (interfaz gráfica)

Telnet o SSH

Consola en RouterOS

Para acceder vía SSH desde la terminal:

~~~sh
ssh admin@192.168.88.1
~~~

2. Configurar una Dirección IP en una Interfaz

Para asignar una dirección IP a una interfaz:

~~~sh
/ip address add address=192.168.1.1/24 interface=ether1
~~~


3. Configurar el Gateway Predeterminado

Para configurar la puerta de enlace predeterminada:

~~~sh
/ip route add gateway=192.168.1.254
~~~

4. Configurar Servidor DHCP

Para configurar un servidor DHCP en una interfaz:
~~~sh
/ip dhcp-server add interface=ether2 address-pool=dhcp_pool disabled=no
/ip dhcp-server network add address=192.168.1.0/24 gateway=192.168.1.1
~~~


5. Configurar NAT para Salida a Internet

Para habilitar NAT y permitir la salida a Internet:
~~~sh
/ip firewall nat add chain=srcnat out-interface=ether1 action=masquerade
~~~


6. Configurar Reglas de Firewall Básicas

Para bloquear tráfico de una IP específica:
~~~sh
/ip firewall filter add chain=input src-address=192.168.1.100 action=drop
~~~


Para permitir tráfico entrante solo desde una red específica:
~~~sh
/ip firewall filter add chain=input src-address=192.168.1.0/24 action=accept
~~~


7. Configurar Usuarios y Contraseñas

Para cambiar la contraseña del usuario admin:
~~~sh
/user set admin password=nueva_clave
~~~


Para agregar un nuevo usuario:
~~~sh
/user add name=usuario password=clave group=full
~~~


8. Configurar Acceso Remoto

Para habilitar el acceso SSH:
~~~sh
/ip service enable ssh
~~~


Para deshabilitar Telnet por seguridad:
~~~sh
/ip service disable telnet
~~~


9. Guardar y Restaurar Configuración

Para guardar la configuración actual:
~~~sh
/export file=config_backup
~~~


Para restaurar desde una copia de seguridad:
~~~sh
/import file=config_backup
~~~


10. Reiniciar y Restablecer el Router

Para reiniciar el router:
~~~sh
/system reboot
~~~


Para restablecer la configuración de fábrica:

~~~sh
/system reset-configuration
~~~
