# [SOLUCION] Nivelador PHP

## 1. Formularios y PHP

~~~php
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario de contacto</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        form {
            background: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        input {
            display: block;
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        button {
            padding: 10px 20px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
    </style>
</head>
<body>
    <form action="procesar_formulario.php" method="post">
        <input type="text" name="nombre" placeholder="Nombre" required>
        <input type="email" name="correo" placeholder="Correo electrónico" required>
        <button type="submit">Enviar</button>
    </form>
</body>
</html>
~~~

**procesar_formulario.php**
~~~php
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = htmlspecialchars($_POST['nombre']);
    $correo = htmlspecialchars($_POST['correo']);
    echo "<p>Gracias, $nombre. Hemos recibido tu correo electrónico: $correo.</p>";
}
?>
~~~

## 2. Manejo de Archivos

~~~php
<?php
$archivo = fopen("datos.txt", "r") or die("No se puede abrir el archivo");
while(!feof($archivo)) {
    echo fgets($archivo) . "<br>";
}
fclose($archivo);
?>
~~~

## 3. Bases de Datos

~~~php
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insertar en la base de datos</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f0f0;
        }
        form {
            background: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        input {
            display: block;
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        button {
            padding: 10px 20px;
            background: #007bff;
            color: #fff;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        button:hover {
            background: #0056b3;
        }
    </style>
</head>
<body>
    <form action="insertar.php" method="post">
        <input type="text" name="nombre" placeholder="Nombre" required>
        <input type="email" name="correo" placeholder="Correo electrónico" required>
        <button type="submit">Insertar</button>
    </form>
</body>
</html>
~~~

**insertar.php**
~~~php
<?php
$servername = "localhost";
$username = "tu_usuario";
$password = "tu_contraseña";
$dbname = "tu_base_de_datos";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $nombre = htmlspecialchars($_POST['nombre']);
    $correo = htmlspecialchars($_POST['correo']);
    
    $stmt = $conn->prepare("INSERT INTO usuarios (nombre, correo) VALUES (:nombre, :correo)");
    $stmt->bindParam(':nombre', $nombre);
    $stmt->bindParam(':correo', $correo);
    
    $stmt->execute();
    echo "Nuevo registro creado con éxito";
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

$conn = null;
?>
~~~

## 4. Sesiones y Cookies

**iniciar_sesion.php**
~~~ php
<?php
session_start();
$_SESSION['nombre'] = "Juan Pérez";
echo "Sesión iniciada. Nombre de usuario: " . $_SESSION['nombre'];
?>
~~~

**mostrar_sesion.php**
~~~php
<?php
session_start();
if(isset($_SESSION['nombre'])) {
    echo "Nombre de usuario: " . $_SESSION['nombre'];
} else {
    echo "No hay sesión iniciada.";
}
?>
~~~

