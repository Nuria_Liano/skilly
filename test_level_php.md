# Nivelador PHP

## 1. Formularios y PHP

HTML:
Crea un formulario HTML que solicite el nombre y el correo electrónico del usuario.

CSS:
Estiliza el formulario para que sea visualmente atractivo.

PHP:
Procesa los datos utilizando PHP y muestra un mensaje de agradecimiento que incluya el nombre del usuario.

## 2. Manejo de Archivos

Pregunta:
¿Cómo se abre, lee y cierra un archivo en PHP?

PHP:
Crea un script PHP que lea el contenido de un archivo de texto llamado "datos.txt" y muestre su contenido en una página web.


## 3. Bases de Datos

Pregunta:
¿Qué es PDO en PHP y por qué se recomienda su uso para trabajar con bases de datos?

HTML:
Crea un formulario HTML que solicite el nombre y el correo electrónico del usuario.

CSS:
Estiliza el formulario.

PHP:
Crea una tabla en una base de datos MySQL con las columnas id, nombre y correo. Luego, escribe un script PHP que inserte un nuevo registro en esta tabla a partir del formulario HTML.


## 4. Sesiones y Cookies

Pregunta:
¿Cuál es la diferencia entre una sesión y una cookie en PHP?

PHP:
Crea un script PHP que inicie una sesión, almacene el nombre del usuario en una variable de sesión y luego muestre el nombre del usuario en diferentes páginas.


