import React from 'react';
import MemoryGame from './MemoryGame';
import './App.css'; // Importa los estilos generales

function App() {
  return (
    <div className="App">
      <MemoryGame />
    </div>
  );
}

export default App;
