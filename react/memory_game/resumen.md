# Resumen React para el juego memory game

## 1. Fundamentos de React

### 1.1 Componentes de React

#### ¿Qué es un Componente en React?

- **Definición**: Un componente en React es una pieza reutilizable de código que representa una parte de la interfaz de usuario. Cada componente puede contener su propia lógica y renderizar parte de la UI. Los componentes en React se pueden crear como funciones o clases.
- **Tipos de Componentes**:
  - **Componentes Funcionales**: Son funciones de JavaScript que aceptan props como argumento y devuelven elementos de React (generalmente en formato JSX). Son más sencillos y se utilizan comúnmente junto con hooks para manejar el estado y los efectos.
  - **Componentes de Clase**: Son clases que extienden `React.Component`. Aunque aún son válidos, los componentes funcionales con hooks son preferidos en la mayoría de los nuevos proyectos.

#### Creación de un Componente Funcional
- **Ejemplo Básico**: Un componente funcional que muestra un mensaje de "Hola, Mundo!".
  ```jsx
  function ComponenteEjemplo() {
    return <div>Hola, Mundo!</div>;
  }
    ```

Explicación: Este componente es una función simple que retorna un fragmento de JSX. Cuando se utiliza en la aplicación, renderiza un div que contiene el texto "Hola, Mundo!".

#### 1.2 JSX (JavaScript XML)

- Definición: JSX es una extensión de la sintaxis de JavaScript que permite escribir código que se parece a HTML directamente en archivos JavaScript. Aunque no es obligatorio, JSX hace que la escritura de la UI en React sea más intuitiva y legible.

- Conceptos Clave:
Interpolación de Variables: Puedes incluir expresiones JavaScript dentro de JSX utilizando llaves {}.
JSX debe tener un elemento raíz: Toda la estructura JSX debe estar envuelta en un único elemento padre.


### 1.2 Props (Propiedades)

Definición: Props son datos que se pasan de un componente padre a un componente hijo

~~~jsx
function Saludo(props) {
  return <h1>Hola, {props.nombre}!</h1>;
}
~~~

### 1.3 Estado (useState)

Definición: El estado es un objeto que contiene datos locales que afectan el renderizado del componente.

Uso del Hook useState:

~~~jsx
import React, { useState } from 'react';

function Contador() {
  const [contador, setContador] = useState(0);

  return (
    <div>
      <p>Has hecho clic {contador} veces</p>
      <button onClick={() => setContador(contador + 1)}>
        Incrementar
      </button>
    </div>
  );
}

~~~

## 2. Manejo de Efectos Secundarios con useEffect

### 2.1 Uso de useEffect

Definición: useEffect es un hook que permite ejecutar efectos secundarios en los componentes, como el manejo de temporizadores, solicitudes a APIs, o manipulación del DOM.

~~~jsx
import React, { useState, useEffect } from 'react';

function EjemploEfecto() {
  const [contador, setContador] = useState(0);

  useEffect(() => {
    document.title = `Has hecho clic ${contador} veces`;
  }, [contador]);

  return (
    <div>
      <p>Has hecho clic {contador} veces</p>
      <button onClick={() => setContador(contador + 1)}>
        Incrementar
      </button>
    </div>
  );
}
~~~

## 3. Renderizado Condicional

### 3.1 Uso de Renderizado Condicional

Definición: El renderizado condicional en React permite mostrar diferentes elementos o componentes en función del estado o las props.

~~~jsx
function MensajeSaludo({ estaLogueado }) {
  if (estaLogueado) {
    return <h1>Bienvenido de nuevo!</h1>;
  }
  return <h1>Por favor, inicie sesión.</h1>;
}
~~~

## 4. Manejo de Eventos en React

### 4.1 Eventos Básicos

Definición: Los eventos en React se manejan de manera similar a los eventos en el DOM, pero con la sintaxis camelCase.

~~~jsx
function Boton() {
  const manejarClick = () => {
    alert('Botón clicado!');
  };

  return <button onClick={manejarClick}>Clic Aquí</button>;
}

~~~

## 5. Estado Avanzado y Manipulación del DOM Virtual

### 5.1 Manipulación de Arrays en el Estado

Actualizar Estado Basado en Arrays: Saber cómo manipular arrays dentro del estado para actualizar el componente.

~~~jsx
function ListaTareas() {
  const [tareas, setTareas] = useState([]);

  const agregarTarea = (tarea) => {
    setTareas([...tareas, tarea]);
  };

  return (
    <div>
      {tareas.map((tarea, index) => (
        <p key={index}>{tarea}</p>
      ))}
      <button onClick={() => agregarTarea('Nueva Tarea')}>
        Agregar Tarea
      </button>
    </div>
  );
}
~~~

### 5.2 Manejo de Efectos Secundarios en Componentes

Temporizadores con useEffect: Saber cómo iniciar y detener temporizadores dentro de un componente React.

~~~jsx
useEffect(() => {
  const timer = setInterval(() => {
    setTime(prevTime => prevTime + 1);
  }, 1000);

  return () => clearInterval(timer);
}, []);

~~~

## 6. Buenas Prácticas en React

### 6.1 Organización de Archivos y Componentes

Separación de Lógica y Estilos: Mantener la lógica de los componentes en archivos .js o .jsx y los estilos en archivos .css.

### 6.2 Uso de key en Listas

Definición: Siempre que renderices listas en React, cada elemento debe tener una key única para optimizar la actualización del DOM virtual.

~~~jsx
{items.map(item => (
  <li key={item.id}>{item.text}</li>
))}
~~~
