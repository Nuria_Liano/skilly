# Memory Game

## Objetivo

El objetivo de esta aplicación es desarrollar un juego de memoria interactivo utilizando la biblioteca React. En este juego, los usuarios deben encontrar parejas de cartas que coincidan, haciendo clic en las cartas para voltearlas. El juego registra el número de intentos y el tiempo que el usuario tarda en completar el juego. Esta aplicación te permitirá practicar y aplicar conceptos clave de React, como el manejo del estado, efectos secundarios, renderizado condicional y eventos.

## Pasos a seguir

### 1. Configurar el Proyecto

### 2. Definir y Manejar el Estado de las Cartas

- Definir los valores de las cartas:

Dentro del componente MemoryGame, define un array con los valores que representarán las cartas (por ejemplo, letras). Duplica y baraja estos valores para crear un conjunto completo de cartas.

- Configurar el estado de las cartas:

Usa useState para manejar las cartas, donde cada carta tiene propiedades para su valor, si está volteada (flipped) y si está emparejada (matched). Esto permite que React reactive el componente cuando cambie el estado, asegurando que la interfaz se actualice correctamente.

- Gestionar las cartas seleccionadas:

Crea un estado adicional para rastrear las cartas actualmente seleccionadas por el usuario. Esto se utiliza para determinar si las dos cartas seleccionadas coinciden.

### 3. Implementar la Lógica de Juego

- Manejo de clics en las cartas:

Implementa la función handleCardClick, que se ejecuta cuando el usuario hace clic en una carta. Esta función debe:
    - Ignorar clics si ya hay dos cartas seleccionadas o si la carta ya está volteada.
    - Voltear la carta seleccionada agregándola a la lista de cartas seleccionadas.
    - Verificar si es la segunda carta seleccionada y, si es así, comparar ambas cartas.
    - Si las cartas coinciden, se marcan como emparejadas.
    - Si no coinciden, se voltean de nuevo después de un breve retraso.

- Contar el número de intentos:

Usa un estado (attempts) para contar cuántos pares de cartas ha intentado emparejar el usuario. Incrementa este contador cada vez que se selecciona un segundo par de cartas.

### 4. Agregar un Temporizador

- Configurar el temporizador:

Añade un estado (time) para rastrear el tiempo que el usuario ha tardado en completar el juego. Usa otro estado (timerActive) para controlar cuándo se debe iniciar o detener el temporizador.

- Implementar el uso del temporizador:

Utiliza useEffect para actualizar el temporizador cada segundo cuando está activo. Esto permite que el tiempo se actualice en tiempo real.

- Iniciar y detener el temporizador:

Inicia el temporizador cuando el usuario voltea la primera carta (handleCardClick). Detén el temporizador cuando todas las cartas estén emparejadas, lo que indica que el juego ha terminado.

### 5. Reiniciar el Juego

- Función de reinicio:

Implementa una función handleResetGame que restaura todas las cartas a su estado inicial, barajándolas de nuevo. Reinicia también el contador de intentos y el temporizador.

- Botón de reinicio:

Añade un botón en la interfaz de usuario que permita al usuario reiniciar el juego en cualquier momento. Esto mejora la experiencia del usuario al ofrecer la posibilidad de reiniciar rápidamente.

### 6. ¡Ponlo bonito!

Crea un archivo CSS (MemoryGame.css) para definir la estructura y la apariencia del juego. Usa CSS Grid para organizar las cartas en una cuadrícula, y aplica animaciones para la acción de voltear las cartas.
Añade animaciones CSS para dar un efecto de volteo a las cartas, lo que mejora la interacción del usuario y hace que el juego sea más visualmente atractivo.