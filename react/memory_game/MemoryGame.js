import React, { useState, useEffect } from 'react';
import './MemoryGame.css'; // Importa los estilos específicos del juego

const MemoryGame = () => {
  // Definir los valores de las cartas
  const cardValues = ["A", "B", "C", "D", "E", "F", "G", "H"];
  
  // Barajar las cartas
  const shuffledCards = [...cardValues, ...cardValues].sort(() => Math.random() - 0.5);

  // Estado para las cartas, cartas seleccionadas, intentos y tiempo
  const [cards, setCards] = useState(shuffledCards.map(value => ({
    value,
    flipped: false,
    matched: false
  })));
  const [selectedCards, setSelectedCards] = useState([]);
  const [attempts, setAttempts] = useState(0);
  const [time, setTime] = useState(0);
  const [timerActive, setTimerActive] = useState(false);

  // Efecto para el temporizador
  useEffect(() => {
    let timer;
    if (timerActive) {
      timer = setInterval(() => {
        setTime(prevTime => prevTime + 1);
      }, 1000);
    } else if (!timerActive && time !== 0) {
      clearInterval(timer);
    }
    return () => clearInterval(timer);
  }, [timerActive]);

  // Manejar clic en las cartas
  const handleCardClick = (index) => {
    if (selectedCards.length === 2 || cards[index].flipped) return;

    const newCards = [...cards];
    newCards[index].flipped = true;
    setSelectedCards([...selectedCards, index]);

    if (!timerActive) {
      setTimerActive(true); // Iniciar temporizador cuando se da vuelta la primera carta
    }

    if (selectedCards.length === 1) {
      setAttempts(attempts + 1);
      const firstIndex = selectedCards[0];
      const secondIndex = index;

      if (newCards[firstIndex].value === newCards[secondIndex].value) {
        // Si las cartas coinciden, marcarlas como emparejadas
        newCards[firstIndex].matched = true;
        newCards[secondIndex].matched = true;
      } else {
        // Si no coinciden, voltearlas nuevamente después de un breve retraso
        setTimeout(() => {
          newCards[firstIndex].flipped = false;
          newCards[secondIndex].flipped = false;
          setCards(newCards);
        }, 1000);
      }
      setSelectedCards([]);
    }
    setCards(newCards);
  };

  // Manejar el reinicio del juego
  const handleResetGame = () => {
    const resetCards = shuffledCards.map(value => ({
      value,
      flipped: false,
      matched: false
    }));
    setCards(resetCards);
    setSelectedCards([]);
    setAttempts(0);
    setTime(0);
    setTimerActive(false);
  };

  // Comprobar si todas las cartas han sido emparejadas
  useEffect(() => {
    if (cards.every(card => card.matched)) {
      setTimerActive(false); // Detener el temporizador si el juego está completado
    }
  }, [cards]);

  return (
    <div className="memory-game">
      <h1>Juego de Memoria</h1>
      <p>Intentos: {attempts}</p>
      <p>Tiempo: {time} segundos</p>
      <button onClick={handleResetGame}>Reiniciar</button>
      <div className="card-grid">
        {cards.map((card, index) => (
          <div
            key={index}
            className={`card ${card.flipped ? 'flipped' : ''}`}
            onClick={() => handleCardClick(index)}
          >
            <div className="card-inner">
              <div className="card-front">?</div>
              <div className="card-back">{card.value}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default MemoryGame;
