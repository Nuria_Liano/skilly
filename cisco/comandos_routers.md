# Comandos para configurar routers | Cisco

## Tipos de routers y usos

### Routers de la serie ISR (Integrated Services Routers)

- Usos:
Diseñados para pequeñas y medianas empresas (PyMEs) y sucursales de empresas grandes.
Ofrecen múltiples servicios en un solo dispositivo, como enrutamiento, firewall, VPN, y optimización WAN.
Integran funciones de colaboración, como voz y vídeo.

- Modelos destacados:
Serie ISR 800: Ideal para pequeñas oficinas y teletrabajadores.
Serie ISR 1000: Compactos, con seguridad integrada y capacidades de conectividad avanzada.
Serie ISR 4000: Diseñados para redes más grandes con mayor capacidad de procesamiento.

### Routers ASR (Aggregation Services Routers)

- Usos:
Diseñados para proveedores de servicios y grandes empresas con necesidades avanzadas de agregación de tráfico.
Manejan grandes volúmenes de datos en redes centrales y en puntos de acceso.
Ofrecen soporte para servicios en la nube, virtualización y capacidades avanzadas de enrutamiento.

- Modelos destacados:
Serie ASR 1000: Adecuado para grandes sucursales y agregación de tráfico empresarial.
Serie ASR 9000: Utilizado en redes de proveedores de servicios y para la agregación en la nube.

### Routers Catalyst

- Usos:
Diseñados para entornos empresariales modernos, especialmente donde se necesita integración de red cableada e inalámbrica.
Soportan automatización avanzada y analítica de red.
Ideales para redes con alta demanda de aplicaciones en tiempo real.

- Modelos destacados:
Serie Catalyst 8000: Solución de última generación para SD-WAN, IoT y conectividad en la nube.
Catalyst 8300 y 8500: Diseñados para grandes empresas y proveedores de servicios.

### Routers de la serie Meraki MX

- Usos:
Diseñados para redes gestionadas en la nube.
Ofrecen capacidades como seguridad avanzada, SD-WAN y fácil administración desde la nube.
Ideales para pequeñas empresas y oficinas con múltiples ubicaciones.

- Modelos destacados:
Meraki MX64/MX67: Para pequeñas oficinas y teletrabajadores.
Meraki MX250/MX450: Diseñados para grandes empresas y centros de datos.

### Routers industriales

- Usos:
Diseñados para entornos industriales y difíciles, como fábricas, transporte y energía.
Ofrecen resistencia a condiciones extremas y conectividad para dispositivos IoT industriales.

- Modelos destacados:
Serie IR1101: Compactos y modulares, ideales para IoT y conectividad industrial.
Serie IR1800: Para aplicaciones industriales críticas, como redes ferroviarias y transporte público.

### Routers para centros de datos

- Usos:
Diseñados para proporcionar conectividad de alto rendimiento en entornos de centros de datos.
Soportan enrutamiento avanzado, virtualización y grandes capacidades de tráfico.

- Modelos destacados:
Cisco Nexus 7000 y 9000: Altamente escalables para centros de datos de próxima generación.

### Routers virtuales (vRouters)

- Usos:
Diseñados para implementaciones en entornos virtuales o en la nube.
Permiten flexibilidad en redes definidas por software (SDN) y servicios en la nube.

- Modelos destacados:
Cisco CSR 1000v: Router virtual para redes en la nube.
Cisco vEdge: Router SD-WAN para redes definidas por software.
### Routers SOHO (Small Office/Home Office)

- Usos:
Diseñados para pequeñas oficinas y teletrabajadores.
Ofrecen conectividad básica con funciones limitadas de seguridad y VPN.

- Modelos destacados:
Cisco RV160/RV260: Routers económicos para redes pequeñas con conectividad básica y funciones VPN.
### Routers de borde

- Usos:
Conectan redes empresariales con la WAN o la Internet pública.
Proveen capacidades avanzadas como SD-WAN, seguridad integrada y optimización de aplicaciones.

- Modelos destacados:
Cisco Edge 340: Diseñado para conectividad en puntos de venta o pequeñas oficinas.
Routers SD-WAN: Para redes definidas por software

## Acceso y configuración inicial

- ``enable``: Cambia al modo EXEC privilegiado.
- ``configure terminal``: Entra al modo de configuración global.
- ``hostname [nombre_del_router]``: Cambia el nombre del router.
- ``exit``: Vuelve al modo anterior.
- ``reload``: Reinicia el router.

2. Configuración de contraseñas
- ``enable secret [contraseña]``: Configura una contraseña cifrada para acceder al modo privilegiado.
- ``line vty 0 4``: Configura las líneas de acceso remoto (Telnet o SSH).
- ``password [contraseña]``: Establece la contraseña para el acceso remoto.
- ``login``: Activa la autenticación.

 Configuración de interfaces
- ``interface [FastEthernet0/0 | GigabitEthernet0/0]``: Entra a la configuración de una interfaz.
- ``ip address [IP] [máscara_subred]``: Asigna una dirección IP a la interfaz.
- ``no shutdown``: Activa la interfaz.
- ``description [texto]``: Añade una descripción a la interfaz.
- ``duplex full``: Configura el modo dúplex completo.
- ``speed [valor]``: Configura la velocidad de la interfaz.
- ``shutdown``: Desactiva la interfaz.

1. Configuración de rutas estáticas
- ``ip route [red_destino] [máscara] [next-hop | interfaz_de_salida]``: Añade una ruta estática.
- ``show ip route``: Muestra la tabla de enrutamiento.

1. Configuración de protocolos de enrutamiento
  OSPF:
- ``router ospf [ID_proceso]``: Activa el protocolo OSPF.
- ``network [red] [máscara_wildcard] area [ID_area]``: Añade redes a OSPF.
  
  EIGRP:
- ``router eigrp [AS]``: Activa el protocolo EIGRP.
- ``network [red] [máscara]``: Añade redes a EIGRP.
RIP:
- ``router rip``: Activa el protocolo RIP.
- ``version 2``: Configura RIP v2.
- ``network [red]``: Añade redes a RIP.

1. Configuración de NAT
- ``ip nat inside source list [ACL_number] interface [interfaz] overload``: Configura NAT con sobrecarga.
- ``access-list [ACL_number] permit [IP] [wildcard]``: Define las reglas ACL para NAT.
- ``interface [nombre_interfaz]``: Configura las interfaces para NAT.
- ``ip nat inside``: Asigna la parte interna de NAT.
- ``ip nat outside``: Asigna la parte externa de NAT

1. Configuración de seguridad
- ``access-list [número | nombre] permit | deny [IP] [wildcard]``: Crea una ACL.
- ``ip access-group [número | nombre] in | out``: Asigna una ACL a una interfaz.
- ``service password-encryption``: Cifra todas las contraseñas en el configurador.


8. Configuración de SSH
- Configura el nombre de dominio: ``ip domain-name [nombre_dominio]``.
- Genera claves RSA: ``crypto key generate rsa modulus [1024 | 2048]``.
- Activa la autenticación de usuario: ``username [nombre] privilege [nivel] secret [contraseña]``.
- Activa SSH en las líneas VTY:

~~~
line vty 0 4
transport input ssh
login local
~~~



1. Configuración de DHCP
- ``ip dhcp pool [nombre_pool]``: Crea un pool DHCP.
- ``network [red] [máscara]``: Define la red asignada.
- ``default-router [IP_router]``: Define el gateway.
- ``dns-server [IP_DNS]``: Define el servidor DNS.
- ``ip dhcp excluded-address [inicio_rango] [fin_rango]``: Excluye direcciones IP

1.  Diagnóstico y verificación
- ``show running-config``: Muestra la configuración actual.
- ``show startup-config``: Muestra la configuración de inicio.
- show ip interface brief: Muestra el estado de las interfaces.
- ``ping [IP_destino]``: Comprueba la conectividad.
- ``traceroute [IP_destino]``: Muestra la ruta hasta el destino.
- ``debug [opción]``: Activa el modo de depuración.
- ``no debug all``: Desactiva todas las depuraciones.