# Nivelador de MariaDB/Mysql

## 1. Crea una base de datos llamada empresa.

~~~sql
CREATE DATABASE empresa;
~~~

## 2. Crea una tabla empleados en la base de datos empresa con los campos: id (INT, AUTO_INCREMENT, PRIMARY KEY), nombre (VARCHAR(100)), apellidos (VARCHAR(100)), salario (DECIMAL(10,2)) y fecha_contratacion (DATE).

~~~sql
CREATE TABLE empleados (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
    apellidos VARCHAR(100),
    salario DECIMAL(10,2),
    fecha_contratacion DATE
);

~~~

## 3. Inserta tres empleados en la tabla empleados.

~~~sql
INSERT INTO empleados (nombre, apellidos, salario, fecha_contratacion) 
VALUES 
('Juan', 'Pérez', 2500.00, '2023-01-15'),
('Ana', 'García', 1800.50, '2022-07-30'),
('Luis', 'Martínez', 3200.75, '2021-11-20');

~~~

## 4. Realiza una consulta que muestre el nombre y apellido de los empleados que tienen un salario mayor a 2000.

~~~sql
SELECT nombre, apellidos 
FROM empleados 
WHERE salario > 2000;

~~~

## 5. Actualiza el salario de un empleado cuyo id es 1, incrementándolo en un 10%.

~~~sql
UPDATE empleados 
SET salario = salario * 1.10 
WHERE id = 1;

~~~

## 6. Elimina de la tabla empleados al empleado cuyo id es 3.

~~~sql
DELETE FROM empleados 
WHERE id = 3;

~~~

## 7. Crea una nueva tabla departamentos con los campos: id_departamento (INT, PRIMARY KEY), nombre_departamento (VARCHAR(50)).

~~~sql
CREATE TABLE departamentos (
    id_departamento INT PRIMARY KEY,
    nombre_departamento VARCHAR(50)
);

~~~

## 8. Inserta dos departamentos en la tabla departamentos.

~~~sql
INSERT INTO departamentos (id_departamento, nombre_departamento) 
VALUES 
(1, 'Recursos Humanos'),
(2, 'Finanzas');

~~~

## 9. Realiza una consulta que muestre todos los empleados junto con el nombre de su departamento (usa un JOIN entre empleados y departamentos).

~~~sql
SELECT empleados.nombre, empleados.apellidos, departamentos.nombre_departamento 
FROM empleados
JOIN departamentos ON empleados.id_departamento = departamentos.id_departamento;

~~~

## 10. Realiza una copia de seguridad de la base de datos empresa utilizando una instrucción de MariaDB.

~~~sql
mysqldump -u root -p empresa > empresa_backup.sql

~~~