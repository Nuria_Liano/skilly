# Resumen CSS

## ¿Qué es CSS?

CSS (Cascading Style Sheets) es un lenguaje utilizado para describir la presentación de un documento escrito en HTML o XML. CSS permite separar el contenido de la presentación, lo que facilita la creación de páginas web atractivas, mantenibles y responsivas.

## ¿Qué se Puede Hacer con CSS?

CSS ofrece una amplia gama de posibilidades para diseñar y mejorar la presentación de las páginas web:

1. **Estilos de Texto:**
   - Cambiar colores, fuentes, tamaños y estilos de texto.
   - Ejemplo: 
     ```css
     p {
         color: blue;
         font-size: 16px;
         font-family: Arial, sans-serif;
     }
     ```

2. **Modelos de Caja:**
   - Controlar el margen, padding, borde y tamaño de los elementos.
   - Ejemplo:
     ```css
     .caja {
         margin: 10px;
         padding: 20px;
         border: 2px solid black;
         width: 200px;
         height: 100px;
     }
     ```

3. **Posicionamiento:**
   - Posicionar elementos de forma estática, relativa, absoluta o fija.
   - Ejemplo:
     ```css
     .absoluto {
         position: absolute;
         top: 50px;
         left: 100px;
     }
     ```

4. **Diseño de Página:**
   - Crear diseños complejos utilizando flexbox, grid y otros métodos de diseño.
   - Ejemplo con Flexbox:
     ```css
     .contenedor {
         display: flex;
         justify-content: space-between;
         align-items: center;
     }
     ```

5. **Estilos Responsivos:**
   - Adaptar el diseño a diferentes tamaños de pantalla mediante media queries.
   - Ejemplo:
     ```css
     @media (max-width: 600px) {
         .contenedor {
             flex-direction: column;
         }
     }
     ```

6. **Animaciones y Transiciones:**
   - Crear animaciones suaves y transiciones entre estados de elementos.
   - Ejemplo de transición:
     ```css
     .boton {
         transition: background-color 0.3s;
     }
     .boton:hover {
         background-color: lightblue;
     }
     ```

## Pseudoclases en CSS

Las pseudoclases en CSS son palabras clave que se añaden a los selectores para especificar un estado especial del elemento seleccionado. Algunas de las más comunes incluyen:

1. **`:hover`:**
   - Se aplica cuando el usuario coloca el cursor sobre un elemento.
   - Ejemplo:
     ```css
     a:hover {
         color: red;
     }
     ```

2. **`:active`:**
   - Se aplica cuando un elemento está siendo activado (por ejemplo, al hacer clic en un botón).
   - Ejemplo:
     ```css
     button:active {
         background-color: green;
     }
     ```

3. **`:focus`:**
   - Se aplica cuando un elemento está enfocado (como un campo de texto seleccionado).
   - Ejemplo:
     ```css
     input:focus {
         border-color: blue;
     }
     ```

4. **`:nth-child(n)`:**
   - Se aplica al enésimo hijo de un elemento padre.
   - Ejemplo:
     ```css
     li:nth-child(odd) {
         background-color: lightgray;
     }
     ```

5. **`:first-child` y `:last-child`:**
   - Se aplican al primer y último hijo de un elemento padre respectivamente.
   - Ejemplo:
     ```css
     p:first-child {
         font-weight: bold;
     }
     p:last-child {
         color: green;
     }
     ```

6. **`:not(selector)`:**
   - Selecciona todos los elementos que no coinciden con el selector dado.
   - Ejemplo:
     ```css
     div:not(.activo) {
         display: none;
     }
     ```

## Pseudoelementos en CSS

Los pseudoelementos en CSS permiten aplicar estilos a partes específicas de un elemento sin necesidad de añadir más HTML. Algunos de los más comunes incluyen:

1. **`::before` y `::after`:**
   - Se utilizan para insertar contenido antes o después del contenido de un elemento. A menudo se usan para añadir decoraciones u otros efectos visuales.
   - Ejemplo:
     ```css
     .boton::before {
         content: '→ ';
         color: blue;
     }
     .boton::after {
         content: ' ←';
         color: blue;
     }
     ```

2. **`::first-line`:**
   - Se aplica a la primera línea de un bloque de texto.
   - Ejemplo:
     ```css
     p::first-line {
         font-weight: bold;
         color: red;
     }
     ```

3. **`::first-letter`:**
   - Se aplica a la primera letra de un bloque de texto.
   - Ejemplo:
     ```css
     p::first-letter {
         font-size: 2em;
         color: green;
     }
     ```

4. **`::selection`:**
   - Se aplica a la parte del documento que el usuario ha seleccionado.
   - Ejemplo:
     ```css
     ::selection {
         background-color: yellow;
         color: black;
     }
     ```

## Selectores en CSS

Los selectores en CSS se utilizan para "seleccionar" los elementos HTML que queremos estilizar. Algunos tipos de selectores importantes son:

1. **Selector de Elemento:**
   - Selecciona todos los elementos de un tipo específico.
   - Ejemplo:
     ```css
     p {
         color: black;
     }
     ```

2. **Selector de Clase:**
   - Selecciona todos los elementos con una clase específica.
   - Ejemplo:
     ```css
     .boton {
         background-color: blue;
         color: white;
     }
     ```

3. **Selector de ID:**
   - Selecciona un elemento único con un ID específico.
   - Ejemplo:
     ```css
     #encabezado {
         font-size: 24px;
     }
     ```

4. **Selectores Atributo:**
   - Selecciona elementos en función del valor de un atributo.
   - Ejemplo:
     ```css
     input[type="text"] {
         border: 1px solid gray;
     }
     ```

5. **Selectores Descendientes y Directos:**
   - Seleccionan elementos dentro de otros elementos.
   - Ejemplo:
     ```css
     .contenedor p {
         margin: 0;
     }
     .contenedor > p {
         color: red;
     }
     ```

## Herencia y Especificidad

### Herencia
Algunas propiedades CSS se heredan de un elemento padre a sus hijos, como el color y la tipografía. Sin embargo, otras propiedades, como el tamaño y los márgenes, no se heredan automáticamente.

### Especificidad
La especificidad determina qué estilos se aplican cuando múltiples reglas coinciden con un elemento. Las reglas más específicas tienen prioridad sobre las menos específicas. La especificidad se calcula basándose en los selectores usados:
- Estilos en línea (`style`): especificidad máxima.
- Selectores ID: alta especificidad.
- Selectores de clase y pseudoclases: especificidad media.
- Selectores de elemento: baja especificidad.

## Modelos de Diseño en CSS

1. **Flexbox:**
   - Ideal para crear diseños de una sola dimensión (una fila o una columna).
   - Ejemplo:
     ```css
     .contenedor {
         display: flex;
         justify-content: space-around;
     }
     ```

2. **CSS Grid:**
   - Mejor para diseños bidimensionales, permitiendo organizar contenido en filas y columnas.
   - Ejemplo:
     ```css
     .grid {
         display: grid;
         grid-template-columns: 1fr 2fr;
         grid-gap: 10px;
     }
     ```

3. **Responsive Design:**
   - Utiliza media queries para adaptar el diseño a diferentes dispositivos y tamaños de pantalla.
   - Ejemplo:
     ```css
     @media (max-width: 768px) {
         .grid {
             grid-template-columns: 1fr;
         }
     }
     ```