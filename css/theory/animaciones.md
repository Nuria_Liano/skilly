# Animaciones en CSS

Las animaciones en CSS permiten agregar movimiento y dinamismo a los elementos de una página web, mejorando la experiencia del usuario. Con CSS, podemos definir cómo un elemento cambia de un estilo a otro a lo largo del tiempo. Hay dos formas principales de animar en CSS: transiciones y animaciones con keyframes.

## Transiciones

Una transición define cómo cambia una propiedad CSS cuando un elemento pasa de un estado a otro. Se utiliza para animar cambios de estilo, como la posición, el color, la opacidad, etc.

~~~css
.elemento {
    transition: propiedad tiempo_funcion_timing retraso;
}
~~~

- propiedad: La propiedad CSS que quieres animar, como background-color, transform, etc.
- tiempo: El tiempo que tarda la transición en completarse, como 0.5s o 2s.
- función timing: Define cómo se desarrolla la transición a lo largo del tiempo (ease, linear, ease-in, ease-out, ease-in-out).
- retraso (opcional): El tiempo que espera antes de iniciar la transición.

**Ejemplo**

En este ejemplo, el color de fondo del botón cambiará suavemente de azul a rojo cuando se coloque el cursor sobre él.

~~~css
.boton {
    background-color: blue;
    transition: background-color 0.3s ease;
}

.boton:hover {
    background-color: red;
}
~~~

## Keyframes

Las animaciones con keyframes permiten más control y complejidad que las transiciones. Los @keyframes definen una secuencia de estilos que un elemento seguirá durante la animación.

~~~css
@keyframes nombre_animacion {
    0% { propiedad: valor_inicial; }
    100% { propiedad: valor_final; }
}

.elemento {
    animation: nombre_animacion duración función_timing retraso número_iteraciones dirección relleno;
}
~~~

- nombre_animacion: El nombre de la animación definida por @keyframes.
- duración: El tiempo que dura la animación (por ejemplo, 2s).
- función timing: Similar a las transiciones (ease, linear, etc.).
- retraso: Tiempo antes de que comience la animación.
- número_iteraciones: Cuántas veces se repite la animación (infinite para que se repita indefinidamente).
- dirección: Puede ser normal, reverse, alternate, o alternate-reverse.
- relleno (fill-mode): Define cómo aplicar los estilos de la animación antes o después de la ejecución (none, forwards, backwards, both).

**Ejemplo**

En este ejemplo, un elemento se desvanece suavemente desde completamente invisible (opacity: 0) hasta completamente visible (opacity: 1) en 2 segundos.

~~~css
@keyframes desvanecer {
    0% { opacity: 0; }
    100% { opacity: 1; }
}

.elemento {
    animation: desvanecer 2s ease-in-out;
}
~~~
