# Nivelador Javascript

## 1. Recoger el valor de un input

HTML:
Crea un formulario con un campo de entrada y un botón.

CSS:
Estiliza el formulario para que sea visualmente atractivo.

JavaScript:
Recoge el valor que el usuario introduce en el campo de entrada cuando se hace clic en el botón y muestra el valor en una alerta.

## 2. Cambiar el color de fondo

HTML:
Crea un botón que cambie el color de fondo de la página cuando se hace clic en él.

CSS:
Define varios colores de fondo posibles.

JavaScript:
Al hacer clic en el botón, cambia el color de fondo de la página de forma aleatoria.

## 3. Validar un formulario

HTML:
Crea un formulario con campos de nombre, correo electrónico y un botón de enviar.

CSS:
Estiliza el formulario.

JavaScript:
Valida que los campos no estén vacíos y que el correo electrónico tenga un formato válido antes de enviar el formulario. Muestra mensajes de error si hay problemas.

## 4. Crear un contador

HTML:
Crea un botón que aumente un contador y otro que lo disminuya.

CSS:
Estiliza los botones y el contador.

JavaScript:
Implementa la lógica para aumentar y disminuir el contador y muestra su valor en pantalla.

## 5. Mostrar/Ocultar contenido

HTML:
Crea un botón que muestre oculte un párrafo de texto.

CSS:
Estiliza el botón y el párrafo.

JavaScript:
Implementa la lógica para mostrar y ocultar el párrafo cuando se hace clic en el botón.