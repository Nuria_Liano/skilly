# Práctica de Procesamiento Paralelo con MPI

## Introducción
En esta práctica se trabajará con **MPI**, una librería estándar para la programación en paralelo en arquitecturas distribuidas. MPI permite la comunicación entre procesos a través de dos modos principales: **comunicación colectiva** y **comunicación punto a punto**.

La programación paralela con MPI es útil para resolver problemas donde se pueden dividir las tareas entre varios procesos que ejecutan código en paralelo.

## Fundamentos de MPI

### ¿Qué es MPI?
MPI (Message Passing Interface) es una librería para la **comunicación entre procesos** en sistemas distribuidos. Los procesos pueden ejecutarse en la misma máquina o en diferentes máquinas conectadas en red.

### Tipos de comunicación en MPI
1. **Comunicación punto a punto**: Implica el envío de un mensaje desde un proceso a otro de manera directa.
2. **Comunicación colectiva**: Involucra a un grupo de procesos que se comunican simultáneamente. Ejemplos incluyen operaciones como broadcast, scatter y gather.

### Ejemplo básico en C con MPI

~~~c
#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv); // Inicializar el entorno MPI

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size); // Número de procesos

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank); // Rango del proceso

    printf("Hola desde el proceso %d de %d\n", world_rank, world_size);

    MPI_Finalize(); // Finalizar el entorno MPI
    return 0;
}
~~~

Este programa inicia un entorno MPI, cada proceso imprime su rango (número único) y el número total de procesos en ejecución.

## Ejercicio 1: Grupos de procesos
### Enunciado
Cada proceso genera un número aleatorio entre 0 y 100. Los procesos cuyo número sea mayor a 50 deben comunicarse entre sí y compartir sus números. Lo mismo sucede con los procesos con números menores o iguales a 50.

**Objetivo**: Al final, el proceso de menor rango en cada grupo debe imprimir la suma de los números del grupo.

### Puntos clave
- Se utiliza **comunicación colectiva** para intercambiar datos entre los procesos.
- Deben crearse dos grupos, uno con números menores a 50 y otro con mayores o iguales a 50.

### Ejemplo de uso de comunicación colectiva
~~~c
// Proceso de comunicación colectiva usando MPI_Gather
MPI_Gather(&mi_numero, 1, MPI_INT, numeros_grupo, 1, MPI_INT, 0, MPI_COMM_WORLD);
~~~

## Ejercicio 2: Comunicación punto a punto
### Enunciado
Dos procesos principales (estáticos) crearán procesos hijos. El proceso hijo del proceso de mayor rango enviará un token aleatorio al otro proceso hijo usando **comunicación punto a punto**.

### Puntos clave
- Uso de `MPI_Send` y `MPI_Recv` para la comunicación entre procesos.
- Los procesos hijos son creados dinámicamente.

### Ejemplo de comunicación punto a punto
~~~c
// Enviar y recibir un mensaje entre dos procesos
if (world_rank == 0) {
    int token = -1;
    MPI_Send(&token, 1, MPI_INT, 1, 0, MPI_COMM_WORLD); // Envío del token
} else if (world_rank == 1) {
    int token;
    MPI_Recv(&token, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // Recepción del token
}
~~~

## Ejercicio 3: Creación de comunicadores
### Enunciado
Dado un grupo de procesos estáticos y dinámicos, se deben crear **intracomunicadores** para agrupar procesos que trabajarán juntos. La tarea implica dividir un grupo de procesos en subgrupos.

### Puntos clave
- Se crea un **intracomunicador** para dividir procesos.
- Los subgrupos trabajan independientemente utilizando su propio comunicador.

### Ejemplo de creación de un comunicador
~~~c
MPI_Comm nuevo_comunicador;
MPI_Comm_split(MPI_COMM_WORLD, color, key, &nuevo_comunicador);
~~~

## Ejercicio 4: Maestro-esclavo
### Enunciado
Un proceso maestro crea entre 5 y 10 procesos dinámicos. El maestro recibe datos desde la consola y los distribuye a los esclavos. Estos los almacenan en un vector y al recibir un valor negativo, envían la suma de los valores almacenados de vuelta al maestro.

### Puntos clave
- La topología de interconexión puede ser una estrella o un anillo.
- Se usa el patrón **maestro-esclavo** para la distribución de tareas.

### Ejemplo de un maestro que envía datos a esclavos
~~~c
if (rank == 0) { // Maestro
    int dato;
    for (int i = 1; i < size; i++) {
        MPI_Send(&dato, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
    }
} else { // Esclavos
    int dato;
    MPI_Recv(&dato, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}
~~~

