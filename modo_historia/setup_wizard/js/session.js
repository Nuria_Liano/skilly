function setSessionItem(key, value) {
    sessionStorage.setItem(key, value);
}

function getSessionItem(key) {
    return sessionStorage.getItem(key);
}

function clearSession() {
    sessionStorage.clear();
}
