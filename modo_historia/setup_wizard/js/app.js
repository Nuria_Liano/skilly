document.addEventListener('DOMContentLoaded', () => {
    initializeForm();
    document.getElementById('calculate-button').addEventListener('click', calculateBudget);
});

async function initializeForm() {
    const components = ['cpu', 'gpu', 'ram', 'ssd', 'hdd', 'usb'];
    for (const component of components) {
        const selectElement = document.getElementById(component);
        const data = await fetchData(component);
        populateSelect(selectElement, data);
    }
}

async function fetchData(componentType) {
    try {
        const response = await fetch(`http://localhost:3000/api/${componentType}`);
        if (!response.ok) {
            throw new Error(`Error: ${response.status} ${response.statusText}`);
        }
        const data = await response.json();
        return data;
    } catch (error) {
        console.error(`Failed to fetch data for ${componentType}:`, error);
        return [];
    }
}

function populateSelect(selectElement, data) {
    data.forEach(item => {
        const option = document.createElement('option');
        option.value = JSON.stringify(item);
        option.textContent = `${item.Brand} ${item.Model} - €${item.Price}`;
        selectElement.appendChild(option);
    });

    selectElement.addEventListener('change', (event) => {
        const selectedData = JSON.parse(event.target.value);
        const imgElement = document.getElementById(`${selectElement.id}-image`);
        imgElement.src = selectedData.imageUrl || 'path/to/default-image.png';
        imgElement.alt = `${selectedData.Brand} ${selectedData.Model}`;
    });
}

function calculateBudget() {
    const components = ['cpu', 'gpu', 'ram', 'ssd', 'hdd', 'usb'];
    let totalPrice = 0;
    let totalPower = 0;

    components.forEach(component => {
        const selectedOption = document.getElementById(component).value;
        if (selectedOption) {
            const selectedData = JSON.parse(selectedOption);
            totalPrice += parseFloat(selectedData.Price);
            totalPower += parseFloat(selectedData['Power (Watts)']);
        }
    });

    document.getElementById('total-price').textContent = `Total precio: €${totalPrice.toFixed(2)}`;
    document.getElementById('total-power').textContent = `Total potencia: ${totalPower}W`;

    // Store results in session storage
    sessionStorage.setItem('totalPrice', totalPrice.toFixed(2));
    sessionStorage.setItem('totalPower', totalPower);

    // Store selected components in cookies
    components.forEach(component => {
        const selectedOption = document.getElementById(component).value;
        document.cookie = `${component}=${selectedOption}; path=/`;
    });
}

// Utility function to get a cookie value by name
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}
