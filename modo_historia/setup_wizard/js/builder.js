document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('build-button').addEventListener('click', buildPC);
    document.getElementById('usage').addEventListener('change', handleUsageChange);
});

function handleUsageChange() {
    const usage = document.getElementById('usage').value;

    document.getElementById('gaming-categories-group').style.display = 'none';
    document.getElementById('simulation-programs-group').style.display = 'none';
    document.getElementById('engineering-programs-group').style.display = 'none';
    document.getElementById('general-programs-group').style.display = 'none';

    if (usage === 'gaming') {
        document.getElementById('gaming-categories-group').style.display = 'block';
    } else if (usage === 'simulation') {
        document.getElementById('simulation-programs-group').style.display = 'block';
    } else if (usage === 'work') {
        document.getElementById('engineering-programs-group').style.display = 'block';
    } else {
        document.getElementById('general-programs-group').style.display = 'block';
    }
}

async function buildPC() {
    const formData = new FormData(document.getElementById('pc-builder-form'));
    const requestData = {
        usage: formData.get('usage'),
        performance: formData.get('performance'),
        budget: formData.get('budget'),
        color: formData.get('color'),
        gamingCategories: formData.get('gaming-categories'),
        simulationPrograms: formData.get('simulation-programs'),
        engineeringPrograms: formData.get('engineering-programs'),
        generalPrograms: formData.get('general-programs'),
        peripherals: formData.get('peripherals'),
        storage: formData.get('storage'),
        upgradability: formData.get('upgradability'),
        quiet: formData.get('quiet'),
        overclock: formData.get('overclock')
    };

    const response = await fetch('http://localhost:3000/api/premade-pc', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(requestData)
    });

    const data = await response.json();
    displayResults(data);
}

function displayResults(data) {
    const componentList = document.getElementById('component-list');
    componentList.innerHTML = '';
    let totalPrice = 0;

    data.forEach(component => {
        const listItem = document.createElement('li');
        const img = document.createElement('img');
        img.src = component.imageUrl;
        img.alt = `${component.Brand} ${component.Model}`;
        img.classList.add('component-image');

        const text = document.createElement('span');
        text.textContent = `${component.Brand} ${component.Model} - €${component.Price}`;

        listItem.appendChild(img);
        listItem.appendChild(text);
        componentList.appendChild(listItem);

        totalPrice += parseFloat(component.Price);
    });

    document.getElementById('total-price').textContent = `Precio total estimado: €${totalPrice.toFixed(2)}`;

    // Store selected components in cookies
    document.cookie = `selectedComponents=${JSON.stringify(data)}; path=/`;
}
