# Setup Wizard 🧙🖥️

![logo setupwizard](img/setupwizard_logo.png)

El objetivo de este proyecto es desarrollar una aplicación web en JavaScript que permita a los usuarios configurar un PC en base a los componentes disponibles en la API proporcionada. La aplicación deberá calcular el precio total y el consumo en vatios del PC configurado. Se utilizarán cookies y sesiones para almacenar y gestionar los datos del usuario y las configuraciones del PC. El proyecto no debe utilizar frameworks y debe hacer uso de técnicas de asincronía para consumir la API.

## Estructura del proyecto

~~~sh
/setupwizard
│
├── /css
│   └── styles.css           // Estilos CSS para la aplicación
├── /img
│   ├── setupwizard_logo.png 
│   └── icons               
│       ├── budget.png
│       ├── builder.png
│       └── premade.png
│
├── /js
│   ├── app.js               // Lógica principal de la aplicación
│   ├── api.js               // Funciones para consumir la API
│   ├── session.js           // Manejo de cookies y sesiones
│   └── utils.js             // Funciones utilitarias
│
├── index.html               // Página principal de la aplicación
├── budget.html              // Página para estimar el presupuesto
├── builder.html             // Página para construir un PC
├── premade.html             // Página para ver PCs premontados
└── README.md                // Documentación del proyecto
~~~

## Descripción de archivos

### index.html

- Página principal de la aplicación.
- Presentación de la herramienta y navegación a otras secciones.

### budget.html

- Contendrá el formulario con los campos necesarios para seleccionar los componentes del PC (CPU, GPU, RAM, SSD, HDD, USB).
- Botones para calcular el precio total y el consumo en vatios.
- Espacios para mostrar los resultados y mensajes de error.

### builder.html

- Formulario basado en preguntas para determinar los componentes adecuados para el usuario.
- Preguntas adicionales como uso principal, rendimiento deseado, presupuesto, etc.
- Generación de una configuración de PC basada en las respuestas.

### premade.html

- Visualización de PCs premontados en formato de tarjetas
- Detalles de los componentes de cada PC y botón de enlace para la compra.

### /js/session.js

- Funciones para manejar cookies y sesiones.
- Almacenamiento y recuperación de datos de configuración de PC en cookies/sesiones.

#### Ejemplo de funciones a crear
- setSessionItem(key, value): Establece un elemento en la sesión con la clave y valor proporcionados.
- getSessionItem(key): Recupera el valor de un elemento de la sesión usando la clave proporcionada.
- clearSession():  Limpia todos los elementos de la sesión.

### /js/utils.js

- Funciones utilitarias como validaciones, formateo de datos, etc.

#### Ejemplo de funciones a crear
- setCookie(name, value, days): Establece una cookie con el nombre, valor y duración en días proporcionados.
- getCookie(name): Obtiene el valor de una cookie por su nombre.
- eraseCookie(name):  Borra una cookie por su nombre.

### /js/app.js

- Lógica principal de la aplicación.
- Manejo de eventos de usuario (clic en botones, selección de componentes).
- Cálculo del precio total y consumo en vatios basado en los datos seleccionados.

#### Ejemplo de funciones a crear
- initializeForm(): Inicializa el formulario obteniendo los datos de componentes de la API y populando los selectores.
- populateSelect(selectElement, data): Llena un elemento de selección (select) con opciones basadas en los datos proporcionados.
- calculateBudget(): Calcula el precio total y el consumo de energía del PC basado en los componentes seleccionados.

### /js/api.js

- Funciones para realizar peticiones a la API de componentes.
- Manejo de respuestas de la API y conversión a formato utilizable por la aplicación.

#### Ejemplo de funciones a crear
- fetchData(componentType): Realiza una petición a la API para obtener datos de componentes por tipo.

### /js/builder.js

- Lógica específica para construir un PC basado en preguntas.
- Manejo de eventos de usuario para cambiar las preguntas adicionales según el uso.
- Enviar los datos recopilados al servidor y mostrar los componentes recomendados.

#### Ejemplo de funciones a crear
- handleUsageChange(): Maneja el cambio de uso principal del PC y muestra preguntas adicionales específicas para ese uso.
- buildPC(): Envía los datos del formulario al servidor para obtener una configuración recomendada del PC.
- displayResults(data): Muestra los componentes recomendados en la interfaz de usuario.

## Ejemplo de peticion y respuesta

**Petición**
~~~sh
curl http://localhost:3000/api/gpu
~~~

**Respuesta**
~~~sh
[
  {
    "Type": "GPU",
    "Part Number": "",
    "Brand": "Nvidia",
    "Model": "RTX 4090",
    "Rank": "1",
    "Benchmark": "370",
    "Samples": "25956",
    "URL": "https://gpu.userbenchmark.com/Nvidia-RTX-4090/Rating/4136",
    "Price": 1499.99,
    "Power (Watts)": 350
  },
  {
    "Type": "GPU",
    "Part Number": "GV-N4090GAMING OC-24GD",
    "Brand": "Gigabyte",
    "Model": "Gigabyte RTX 4090 24GB Gaming OC",
    "Rank": "1",
    "Benchmark": "370",
    "Samples": "25956",
    "URL": "https://gpu.userbenchmark.com/Nvidia-RTX-4090/Rating/4136",
    "Price": 1599.99,
    "Power (Watts)": 360
  }
]
~~~