<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php include 'includes/header.php'; ?>

    <div class="form-container">
        <h2>Bienvenido a tu gestor de playlists</h2>
        <p>Explora, organiza y descubre nueva música.</p>
        <a href="playlists.php"><button>Ver Mis Playlists</button></a>
    </div>

    <?php include 'includes/footer.php'; ?>
</body>
</html>