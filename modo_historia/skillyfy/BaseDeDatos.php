<?php
class BaseDeDatos {
    private $host = 'localhost';
    private $dbname = 'skillyfy';
    private $usuario = 'root';
    private $password = '';
    private $conexion;

    // Función para conectar a la base de datos utilizando PDO
    public function conectar() {
        try {
            $this->conexion = new PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->usuario, $this->password);
            // Establecer el modo de error de PDO a excepción
            $this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->conexion;
        } catch (PDOException $e) {
            echo "Error de conexión: " . $e->getMessage();
            return null;
        }
    }

    // Función para ejecutar consultas preparadas
    public function ejecutarConsulta($sql, $parametros = []) {
        try {
            $stmt = $this->conexion->prepare($sql);  // Prepara la consulta
            if (!empty($parametros)) {
                // Si hay parámetros, los enlaza a la consulta preparada
                foreach ($parametros as $clave => $valor) {
                    $stmt->bindValue($clave, $valor);
                }
            }
            $stmt->execute();  // Ejecuta la consulta
            return $stmt;  // Retorna el statement para poder trabajar con los resultados si es necesario
        } catch (PDOException $e) {
            echo "Error en la consulta: " . $e->getMessage();
            return false;
        }
    }

        // Función para crear la base de datos y sus tablas
    public function crearBaseDeDatos() {
        try {
            // Crear la base de datos si no existe
            $sql = "CREATE DATABASE IF NOT EXISTS skillyfy";
            $this->conexion->exec($sql);

            // Usar la base de datos recién creada
            $sql = "USE skillyfy";
            $this->conexion->exec($sql);

            // Crear tabla usuarios
            $sql = "
            CREATE TABLE IF NOT EXISTS usuarios (
                id INT AUTO_INCREMENT PRIMARY KEY,
                nombre VARCHAR(100) NOT NULL,
                email VARCHAR(100) NOT NULL UNIQUE,
                password VARCHAR(255) NOT NULL,
                creado_en TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            )";
            $this->conexion->exec($sql);

            // Crear tabla playlists
            $sql = "
            CREATE TABLE IF NOT EXISTS playlists (
                id INT AUTO_INCREMENT PRIMARY KEY,
                nombre VARCHAR(100) NOT NULL,
                id_usuario INT NOT NULL,
                portada VARCHAR(255) DEFAULT NULL,
                creado_en TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                FOREIGN KEY (id_usuario) REFERENCES usuarios(id) ON DELETE CASCADE
            )";
            $this->conexion->exec($sql);

            // Crear tabla canciones
            $sql = "
            CREATE TABLE IF NOT EXISTS canciones (
                id INT AUTO_INCREMENT PRIMARY KEY,
                titulo VARCHAR(100) NOT NULL,
                artista VARCHAR(100) NOT NULL,
                archivo VARCHAR(255) DEFAULT NULL,
                id_playlist INT NOT NULL,
                agregado_en TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                FOREIGN KEY (id_playlist) REFERENCES playlists(id) ON DELETE CASCADE
            )";
            $this->conexion->exec($sql);

            // echo "Base de datos y tablas creadas con éxito.";
        } catch (PDOException $e) {
            echo "Error al crear la base de datos: " . $e->getMessage();
        }
    }

    // Función para cerrar la conexión a la base de datos
    public function desconectar() {
        $this->conexion = null;  // Establece la conexión como null para cerrarla
    }
}
?>
