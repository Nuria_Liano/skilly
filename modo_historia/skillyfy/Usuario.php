<?php
class Usuario {
    private $db;

    // Constructor para recibir la conexión a la base de datos
    public function __construct($conexionDB) {
        $this->db = $conexionDB;
    }

    public function comprobarUsuario($email){
        $sql = "SELECT * FROM usuarios WHERE email = :email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        
        return $stmt;
    }

    // Función para registrar un nuevo usuario
    public function registrarUsuario($nombre, $email, $password) {
        // Verificar si el email ya existe
        $stmt = $this->comprobarUsuario($email);
        if ($stmt->rowCount() > 0) {
            return "El email ya está registrado.";
        }

        // Encriptar la contraseña
        $passwordHash = password_hash($password, PASSWORD_BCRYPT);

        // Insertar el nuevo usuario en la base de datos
        $sql = "INSERT INTO usuarios (nombre, email, password) VALUES (:nombre, :email, :password)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $passwordHash);
        
        if ($stmt->execute()) {
            return "Usuario registrado con éxito.";
        } else {
            return "Error al registrar el usuario.";
        }
    }

    // Función para iniciar sesión
    public function iniciarSesion($email, $password) {
        // Buscar el usuario en la base de datos

        $stmt = $this->comprobarUsuario($email);

        if ($stmt->rowCount() === 1) {
            $usuario = $stmt->fetch(PDO::FETCH_ASSOC);
            // Verificar la contraseña
            if (password_verify($password, $usuario['password'])) {
                // Iniciar la sesión del usuario
                session_start();
                $_SESSION['id_usuario'] = $usuario['id'];
                $_SESSION['nombre'] = $usuario['nombre'];
                return "Sesión iniciada correctamente.";
            } else {
                return "Contraseña incorrecta.";
            }
        } else {
            return "El email no está registrado.";
        }
    }

    // Función para cerrar sesión
    public function cerrarSesion() {
        session_start();
        session_unset();
        session_destroy();
        header("Location: index.php");
    }

    // Función para editar el perfil del usuario
    public function editarPerfil($idUsuario, $nuevoNombre, $nuevoEmail) {
        // Verificar si el nuevo email ya está en uso por otro usuario
        $sql = "SELECT * FROM usuarios WHERE email = :email AND id != :idUsuario";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $nuevoEmail);
        $stmt->bindParam(':idUsuario', $idUsuario);
        $stmt->execute();
        
        if ($stmt->rowCount() > 0) {
            return "El nuevo email ya está en uso por otro usuario.";
        }

        // Actualizar los datos del usuario en la base de datos
        $sql = "UPDATE usuarios SET nombre = :nombre, email = :email WHERE id = :idUsuario";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':nombre', $nuevoNombre);
        $stmt->bindParam(':email', $nuevoEmail);
        $stmt->bindParam(':idUsuario', $idUsuario);
        
        if ($stmt->execute()) {
            return "Perfil actualizado con éxito.";
        } else {
            return "Error al actualizar el perfil.";
        }
    }
}
?>


