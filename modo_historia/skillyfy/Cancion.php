<?php
class Cancion {
    private $db;

    // Constructor para recibir la conexión a la base de datos
    public function __construct($conexionDB) {
        $this->db = $conexionDB;
    }

    // Función para agregar una canción a una playlist
    public function agregarCancionAPlaylist($idPlaylist, $titulo, $artista, $archivoCancion = null) {
        try {
            $sql = "INSERT INTO canciones (id_playlist, titulo, artista, archivo) VALUES (:idPlaylist, :titulo, :artista, :archivo)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':idPlaylist', $idPlaylist);
            $stmt->bindParam(':titulo', $titulo);
            $stmt->bindParam(':artista', $artista);
            $stmt->bindParam(':archivo', $archivoCancion);

            if ($stmt->execute()) {
                return "Canción agregada a la playlist con éxito.";
            } else {
                return "Error al agregar la canción a la playlist.";
            }
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para eliminar una canción de una playlist
    public function eliminarCancionDePlaylist($idCancion) {
        try {
            $sql = "DELETE FROM canciones WHERE id = :idCancion";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':idCancion', $idCancion);

            if ($stmt->execute()) {
                return "Canción eliminada con éxito.";
            } else {
                return "Error al eliminar la canción.";
            }
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para listar canciones de una playlist
    public function listarCancionesDePlaylist($idPlaylist) {
        try {
            $sql = "SELECT * FROM canciones WHERE id_playlist = :idPlaylist";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':idPlaylist', $idPlaylist);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para buscar canciones dentro de una playlist
    public function buscarCancionEnPlaylist($idPlaylist, $criterio) {
        try {
            $sql = "SELECT * FROM canciones WHERE id_playlist = :idPlaylist AND (titulo LIKE :criterio OR artista LIKE :criterio)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':idPlaylist', $idPlaylist);
            $criterioBusqueda = "%" . $criterio . "%";
            $stmt->bindParam(':criterio', $criterioBusqueda);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }
}
?>
