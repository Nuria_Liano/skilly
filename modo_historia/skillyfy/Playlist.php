<?php
class Playlist {
    private $db;

    // Constructor para recibir la conexión a la base de datos
    public function __construct($conexionDB) {
        $this->db = $conexionDB;
    }

    // Función para crear una nueva playlist
    public function crearPlaylist($idUsuario, $nombrePlaylist) {
        try {
            $sql = "INSERT INTO playlists (nombre, id_usuario) VALUES (:nombre, :idUsuario)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':nombre', $nombrePlaylist);
            $stmt->bindParam(':idUsuario', $idUsuario);
            
            if ($stmt->execute()) {
                return "Playlist creada con éxito.";
            } else {
                return "Error al crear la playlist.";
            }
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para eliminar una playlist y sus canciones asociadas
    public function eliminarPlaylist($idPlaylist) {
        try {
            // Eliminar canciones asociadas a la playlist
            $sqlCanciones = "DELETE FROM canciones WHERE id_playlist = :idPlaylist";
            $stmtCanciones = $this->db->prepare($sqlCanciones);
            $stmtCanciones->bindParam(':idPlaylist', $idPlaylist);
            $stmtCanciones->execute();

            // Eliminar la playlist
            $sqlPlaylist = "DELETE FROM playlists WHERE id = :idPlaylist";
            $stmtPlaylist = $this->db->prepare($sqlPlaylist);
            $stmtPlaylist->bindParam(':idPlaylist', $idPlaylist);
            
            if ($stmtPlaylist->execute()) {
                return "Playlist eliminada con éxito.";
            } else {
                return "Error al eliminar la playlist.";
            }
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para editar el nombre de una playlist
    public function editarNombrePlaylist($idPlaylist, $nuevoNombre) {
        try {
            $sql = "UPDATE playlists SET nombre = :nuevoNombre WHERE id = :idPlaylist";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':nuevoNombre', $nuevoNombre);
            $stmt->bindParam(':idPlaylist', $idPlaylist);
            
            if ($stmt->execute()) {
                return "Nombre de la playlist actualizado con éxito.";
            } else {
                return "Error al actualizar el nombre de la playlist.";
            }
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para obtener las playlists de un usuario
    public function obtenerPlaylistsPorUsuario($idUsuario) {
        try {
            $sql = "SELECT * FROM playlists WHERE id_usuario = :idUsuario";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':idUsuario', $idUsuario);
            $stmt->execute();
            
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }

    // Función para subir la portada de una playlist
    public function subirPortada($idPlaylist, $archivoImagen) {
        $nombreArchivo = basename($archivoImagen['name']);
        $directorioDestino = "portadas/";
        $rutaArchivo = $directorioDestino . $nombreArchivo;
        $tipoArchivo = strtolower(pathinfo($rutaArchivo, PATHINFO_EXTENSION));
        $tamañoArchivo = $archivoImagen['size'];

        // Verificar formato de imagen
        $formatosPermitidos = ['jpg', 'jpeg', 'png', 'gif'];
        if (!in_array($tipoArchivo, $formatosPermitidos)) {
            return "Formato de imagen no permitido. Solo se permiten archivos JPG, JPEG, PNG y GIF.";
        }

        // Verificar tamaño de archivo (por ejemplo, máximo 2MB)
        if ($tamañoArchivo > 2000000) {
            return "El archivo es demasiado grande. Tamaño máximo permitido es 2MB.";
        }

        // Mover el archivo al servidor
        if (move_uploaded_file($archivoImagen['tmp_name'], $rutaArchivo)) {
            // Actualizar la ruta de la portada en la base de datos
            try {
                $sql = "UPDATE playlists SET portada = :portada WHERE id = :idPlaylist";
                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':portada', $rutaArchivo);
                $stmt->bindParam(':idPlaylist', $idPlaylist);
                
                if ($stmt->execute()) {
                    return "Portada subida con éxito.";
                } else {
                    return "Error al actualizar la portada en la base de datos.";
                }
            } catch (PDOException $e) {
                return "Error: " . $e->getMessage();
            }
        } else {
            return "Error al subir el archivo.";
        }
    }
}
?>
