# Skillyfy. Gestor de Playlists Musicales con Recomendaciones

## Objetivo

Desarrolla una aplicación en PHP orientada a objetos que permita a los usuarios crear y gestionar playlists musicales personalizadas, incluyendo recomendaciones automáticas a través de una API musical.

## Clases principales

### Clase `Usuario`

Esta clase gestiona la información y las acciones relacionadas con los usuarios de la aplicación

**Funciones**

- **registrarUsuario**
  - Registra un nuevo usuario en la base de datos, encriptando la contraseña.
  - Valida que el email no esté ya registrado.
- **iniciarSesion**
  - Verifica las credenciales del usuario y, si son correctas, inicia la sesión.
  - Utiliza funciones PHP de manejo de sesiones.
- **cerrarSesion**
  - Finaliza la sesión actual del usuario.
  - Destruye la sesión activa y redirige al usuario a la página de inicio.
- **editarPerfil**
  - Permite al usuario actualizar su perfil, cambiando su nombre y/o email.
  - Verifica que el nuevo email no esté en uso por otro usuario.

### Clase `Playlist`

Esta clase maneja las acciones relacionadas con la creación y gestión de las playlists del usuario.

**Funciones**

- **crearPlaylist**
  - Crea una nueva playlist asociada al usuario que la solicita.
  - Inserta el registro de la nueva playlist en la base de datos.
- **eliminarPlaylist**
  - Elimina una playlist específica de la base de datos.
  - Elimina todas las canciones asociadas a la playlist.
- **editarNombrePlaylist**
  - Cambia el nombre de la playlist en la base de datos.
- **obtenerPlaylistsPorUsuario**
  - Devuelve todas las playlists asociadas a un usuario en particular.
- **subirPortada**
  - Permite al usuario subir una imagen de portada para una playlist.
  - Verifica el formato y tamaño de la imagen, y la guarda en el servidor.

### Clase `Cancion`

Esta clase gestiona las canciones dentro de las playlists.

**Funciones**

- **agregarCancionAPlaylist**
  - Añade una canción a una playlist específica.
  - Permite subir un archivo de audio si es local, o almacenar un enlace si es una canción en línea.
- **eliminarCancionDePlaylist**
  - Elimina una canción específica de una playlist.
- **listarCancionesDePlaylist**
  - Devuelve una lista de todas las canciones dentro de una playlist.
- **buscarCancionEnPlaylist**
  - Busca canciones dentro de una playlist según un criterio de búsqueda (por título o artista).

### Clase `RecomendadorMusical`

Esta clase se encarga de consumir una API musical (como Spotify o Last.fm) para generar recomendaciones de canciones basadas en las canciones ya añadidas a la playlist.

**Funciones**

- **obtenerRecomendaciones**
  - Se conecta a la API musical y solicita recomendaciones basadas en el género o artista de las canciones ya agregadas a la playlist.
  - Devuelve un listado de al menos tres canciones recomendadas.
- **mostrarRecomendaciones**
  - Genera una lista de recomendaciones para el usuario en función de las canciones de la playlist seleccionada.
  - Llama a obtenerRecomendaciones() para obtener canciones relacionadas.
- **listarCancionesDePlaylist**
  - Devuelve una lista de todas las canciones dentro de una playlist.

### Clase `BaseDeDatos`

Esta clase maneja la conexión con la base de datos y las operaciones CRUD necesarias para la aplicación.

**Funciones**

- **conectar**
  - Establece la conexión con la base de datos utilizando PDO.
- **ejecutarConsulta**
  - Ejecuta consultas SQL preparadas (SELECT, INSERT, UPDATE, DELETE) en la base de datos.
  - Utiliza parámetros para evitar inyecciones SQL.
- **desconectar**
  - Cierra la conexión con la base de datos

## Interfaz gráfica

### Navegación

Durante toda la aplicación tiene que aparecer este menú de navegación

- Menú superior: Un menú fijo con opciones para "Inicio", "Mis Playlists", "Perfil" y "Cerrar Sesión".

### Página de Registro e Inicio de Sesión

Permitir que los usuarios se registren y accedan a la aplicación.

**Formulario de registro**

- Campos: Nombre, Email, Contraseña, Confirmar Contraseña.
- Botón para "Registrarse".

**Formulario de inicio de sesión**

- Campos: Email, Contraseña.
- Botón para "Iniciar Sesión".

### Página Index (Lista de Playlists)

Mostrar las playlists del usuario con opciones para crear, editar y eliminar playlists.

**Listado de playlists**

- Una lista de tarjetas (cards) donde cada tarjeta representa una playlist.
- Botón "Crear Nueva Playlist".

### Página de Gestión de una Playlist

Mostrar las canciones dentro de una playlist, permitir agregar nuevas canciones y ver las recomendaciones musicales.

**Listado de canciones**

- Cada canción se muestra con su título y artista.
- Botones "Eliminar" y "Agregar Canción".

**Formulario para agregar canciones**

- Campos: Título, Artista, Archivo (opcional).
- Botón para "Agregar canción".

**Sección de recomendaciones**

- Lista de canciones sugeridas con un botón para agregar cada una a la playlist.

### Página de Perfil del Usuario

Permitir al usuario editar su perfil, cambiar nombre, email y contraseña.

**Formulario de edición de perfil**

- Campos: Nombre, Email, Contraseña actual, Nueva contraseña, Confirmar nueva contraseña.
- Botón para "Guardar Cambios".

## Flujo de la aplicación

1. Los usuarios pueden registrarse e iniciar sesión utilizando la clase Usuario.
2. Una vez registrados, pueden crear y gestionar sus playlists a través de la clase Playlist, que les permite agregar canciones, subir portadas y editar los nombres de sus listas.
3. Las canciones pueden ser agregadas o eliminadas dentro de las playlists con la clase Cancion, que también maneja la subida de archivos y la búsqueda de canciones dentro de una lista.
4. El usuario puede recibir recomendaciones automáticas basadas en sus playlists, gracias a la clase RecomendadorMusical, que conecta con la API externa para sugerir canciones.
5. Toda la información se almacena y gestiona mediante la clase BaseDeDatos, que permite operar sobre las tablas de usuarios, playlists y canciones.

## Recursos

APIs para integrar una funcionalidad de recomendaciones de canciones en tu proyecto

- [Spotify Web API](https://developer.spotify.com/documentation/web-api)
- [Last.fm API](https://www.last.fm/api)
- [Deezer API](https://developers.deezer.com/login?redirect=/api)