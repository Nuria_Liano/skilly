<?php
class RecomendadorMusical {
    private $apiKey;
    private $apiUrl;

    // Constructor para configurar la API (por ejemplo, Spotify o Last.fm)
    public function __construct($apiUrl, $apiKey) {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
    }

    // Función para obtener recomendaciones basadas en canciones de una playlist
    public function obtenerRecomendaciones($canciones) {
        $recomendaciones = [];

        foreach ($canciones as $cancion) {
            $titulo = urlencode($cancion['titulo']);
            $artista = urlencode($cancion['artista']);

            $url = "{$this->apiUrl}?track={$titulo}&artist={$artista}&api_key={$this->apiKey}&format=json";

            $respuesta = file_get_contents($url);
            if ($respuesta !== false) {
                $datos = json_decode($respuesta, true);
                if (isset($datos['similartracks']['track'])) {
                    foreach ($datos['similartracks']['track'] as $track) {
                        $recomendaciones[] = [
                            'titulo' => $track['name'],
                            'artista' => $track['artist']['name'],
                            'url' => $track['url'] ?? null,
                        ];
                    }
                }
            }
        }

        return $recomendaciones;
    }

    // Función para mostrar recomendaciones en base a una playlist
    public function mostrarRecomendaciones($idPlaylist, $conexionDB) {
        try {
            // Obtener las canciones de la playlist
            $sql = "SELECT titulo, artista FROM canciones WHERE id_playlist = :idPlaylist";
            $stmt = $conexionDB->prepare($sql);
            $stmt->bindParam(':idPlaylist', $idPlaylist);
            $stmt->execute();
            $canciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (empty($canciones)) {
                return "No hay canciones en la playlist para generar recomendaciones.";
            }

            // Obtener recomendaciones basadas en las canciones
            $recomendaciones = $this->obtenerRecomendaciones($canciones);

            if (empty($recomendaciones)) {
                return "No se encontraron recomendaciones para esta playlist.";
            }

            return $recomendaciones;
        } catch (PDOException $e) {
            return "Error: " . $e->getMessage();
        }
    }
}
?>
