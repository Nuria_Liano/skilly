# Proyecto: Clima y Recomendación de Ropa

## Descripción del Proyecto
Crea una aplicación web en JavaScript que consulte el clima en tiempo real y ofrezca recomendaciones de ropa y accesorios según las condiciones meteorológicas. La aplicación debe permitir al usuario ingresar una ubicación (ciudad o coordenadas) y obtener la temperatura, humedad, condiciones actuales (como lluvia, sol o nieve) y una recomendación sobre qué tipo de ropa usar. Utiliza una API pública de clima para obtener los datos y aplica conceptos avanzados como asincronía, manejo de errores y control de estado de carga (loading).

## Requisitos Funcionales

1. **Búsqueda de Clima**
   - El usuario debe poder ingresar el nombre de una ciudad o las coordenadas (latitud y longitud) para obtener el clima en esa ubicación.

2. **Consumo de API Pública**
   - Utiliza una API pública de clima (por ejemplo, [OpenWeatherMap](https://openweathermap.org/)) para obtener los datos meteorológicos en tiempo real.

3. **Manejo Asíncrono de Datos**
   - Usa `async/await` para realizar las solicitudes a la API y manejar las respuestas de manera eficiente.

4. **Recomendación de Ropa y Accesorios**
   - La aplicación debe dar sugerencias de vestimenta según el clima:
     - **Temperatura baja**: Recomienda abrigos, guantes, bufanda.
     - **Lluvia**: Sugiere paraguas o impermeable.
     - **Sol intenso**: Recomienda bloqueador solar, gafas de sol y ropa ligera.

5. **Estado de Carga (Loading)**
   - Muestra un mensaje de carga mientras la aplicación obtiene los datos de la API.

6. **Manejo de Errores**
   - Implementa una gestión de errores para mostrar mensajes claros cuando:
     - La ciudad o coordenadas no se encuentren.
     - La API falle o haya problemas de red.

7. **Interfaz Amigable y Adaptable**
   - La interfaz debe ser intuitiva y presentarse bien tanto en dispositivos móviles como en desktops.

## Extras Opcionales

- **Historial de Búsqueda**
  - Agrega un historial de búsqueda para que el usuario pueda ver el clima de sus últimas consultas.

- **Sistema de Favoritos**
  - Permite que los usuarios guarden sus ubicaciones preferidas.

- **Gráficos de Evolución del Clima**
  - Si la API lo permite, añade gráficos simples que muestren la evolución de la temperatura de los últimos días.

## Objetivos de Aprendizaje

- Comprender el uso de **APIs públicas** y cómo integrarlas en proyectos web.
- Aplicar **asincronía** (`async/await` y `fetch`) para realizar solicitudes y manejar datos externos.
- Aprender a **manejar errores** y dar retroalimentación al usuario en casos de problemas.
- Practicar la construcción de **interfaces dinámicas** y amigables.

Este proyecto expondrá a los estudiantes a conceptos avanzados de JavaScript mientras crean una herramienta práctica y útil.
