# Simulación de una Batalla de Rol (RPG)

El objetivo de esta práctica es crear un programa que simule una batalla en un juego de rol (RPG) con varios
tipos de personajes, pociones y eventos de batalla. Utilizaremos la jerarquía de clases y los elementos de juego
descritos en el siguiente diagrama:

![alt text](diagrama.png)

## Descripción del funcionamiento del programa

### Clase Juego

- Cuando se inicia el juego el primer paso será inciializar una partida con 1 mago, 1 héroe y 6 orcos. El
juego contiene una lista de personajes en la que se encontrarán almacenados los 8 personajes.
- A partir de este momento, por cada ronda de juego cada personaje (el mago, el héroe y los 6 orcos)
jugarán un turno. Para ello se utilizará el polimorfismo y el método step(), que cada tipo de jugador
implentará de una manera diferente.
- El bucle principal de las rondas de juego se ejecutará mientras haya orcos vivos (si mueren todos, el
jugador ha ganado) o hasta que muera el héroe (el jugador habrá perdido).

### Clase Personaje

- Se trata de una clase abstracta en la cual se implementan los métodos que son comunes a todos los
personajes (como recibirAtaque, tomarPoción o cogerPoción) y en la que se definen métodos
abstractos que los 3 tipos de personajes han de implementar de una manera específica.

### Clase Mago:

- En su creación se otrogarán los 300 puntos de vida, 30 puntos de ataque y 300 puntos como máximo
de puntos de vida. Además se inicializará su lista de pociones con 6 pociones (dos de cada tipo).
- En su step se decidirá entre atacar a un orco, tomar una poción (si el mago tiene menos de 30 puntos
de vida) o dar una poción al héroe (si tiene 10 puntos de vida o menos).

### Clase Orco:

- Para crear un orco se generará un número aleatorio entre 0 y 1 que actuará de multiplicador de los
puntos de vida y de los puntos de ataque.
- En su creación se otrogarán 100*r puntos de vida, 20*r puntos de ataque y 100*r puntos como
máximo de puntos de vida. Además se inicializará su lista de pociones con 0 pociones.
- En su step se decidirá entre atacar al héroe, atacar al mago, tomar una poción o recoger una poción
del suelo. Para decidirlo se generará un número aleatorio. Si el número es mayor que 0.5, se atacará
al héroe, si el número está entre 0.5 y 0.25, se atacará al mago y, si el número es menor que 0.25, se
tomará o cogerá una poción (si hay pociones en la lista se tomará una, si está vacía se cogerá una).

### Clase Poción:

- Esta clase abstracta tendrá un método abstracto para usar la poción que, dependiendo del
tipo se implementará de una manera específica. Las pociones de vida aumentan en 10 los
puntos de vida, las pociones de ataque aumentarán en 2 los puntos de ataque, las pociones
de nivel aumentarán los puntos de vida máximos del personaje en 20 puntos.

## Consideraciones

- Utiliza la herencia para evitar la duplicación de código en las clases derivadas.
- Utiliza el encapsulamiento para controlar el acceso a los atributos de las clases.
- Es muy importante el uso del polimorfismo y evitar comprobar a qué clase pertenecen los objetos con
instance of.
