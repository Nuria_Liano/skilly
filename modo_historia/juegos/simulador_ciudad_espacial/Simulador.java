package modo_historia.juegos.simulador_ciudad_espacial;

import java.util.Scanner;

public class Simulador {
    public static void main(String[] args) {
        Ciudad ciudad = new Ciudad();
        Scanner scanner = new Scanner(System.in);

        // Ejemplo de inicialización de colonos
        ciudad.agregarColono(new Colono("Colono 1", "Agricultor"));
        ciudad.agregarColono(new Colono("Colono 2", "Ingeniero"));
        ciudad.agregarColono(new Colono("Colono 3", "Operario"));

        boolean juegoActivo = true;
        while (juegoActivo) {
            System.out.println("\nSeleccione una acción:");
            System.out.println("1. Construir Edificio");
            System.out.println("2. Asignar Colono a Edificio");
            System.out.println("3. Desasignar Colono de Edificio");
            System.out.println("4. Pasar Turno");
            System.out.println("5. Mostrar Mapa");
            System.out.println("6. Mostrar Estado de Recursos");
            System.out.println("7. Mostrar Información de Colonos");
            System.out.println("8. Salir");

            int opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("Ingrese las coordenadas x e y para construir (0-9): ");
                    int x = scanner.nextInt();
                    int y = scanner.nextInt();
                    System.out.println("Ingrese el nombre del edificio: ");
                    String nombreEdificio = scanner.next();
                    System.out.println("Ingrese el tipo de edificio (Granja, Central Energética, Fábrica): ");
                    String tipoEdificio = scanner.next();
                    System.out.println("Ingrese el coste en materiales: ");
                    int coste = scanner.nextInt();
                    ciudad.construirEdificio(x, y, new Edificio(nombreEdificio, coste, tipoEdificio));
                    break;

                case 2:
                    System.out.println("Ingrese el índice del colono: ");
                    int colonoIndex = scanner.nextInt();
                    System.out.println("Ingrese el índice del edificio: ");
                    int edificioIndex = scanner.nextInt();
                    ciudad.asignarColonoAEdificio(colonoIndex, edificioIndex);
                    break;

                case 3:
                    System.out.println("Ingrese el índice del colono para desasignar: ");
                    colonoIndex = scanner.nextInt();
                    Colono colono = ciudad.getColono(colonoIndex);
                    if (colono != null && colono.isAsignado()) {
                        colono.desasignar();
                    } else {
                        System.out.println("El colono no está asignado a ningún edificio.");
                    }
                    break;

                case 4:
                    ciudad.pasarTurno();
                    break;

                case 5:
                    ciudad.mostrarMapa();
                    break;

                case 6:
                    ciudad.mostrarEstadoRecursos();
                    break;

                case 7:
                    ciudad.mostrarInformacionColonos();
                    break;

                case 8:
                    juegoActivo = false;
                    break;

                default:
                    System.out.println("Opción inválida.");
            }
        }

        scanner.close();
        System.out.println("Fin del juego.");
    }
}
