package modo_historia.juegos.simulador_ciudad_espacial;
import java.util.ArrayList;

public class Edificio {
    private String nombre;
    private int costeMateriales;
    private ArrayList<Colono> colonosAsignados = new ArrayList<>();
    private String tipo; // Tipo de edificio: "Granja", "Central Energética", "Fábrica", etc.

    public Edificio(String nombre, int costeMateriales, String tipo) {
        this.nombre = nombre;
        this.costeMateriales = costeMateriales;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCosteMateriales() {
        return costeMateriales;
    }

    public String getTipo() {
        return tipo;
    }

    public void asignarColono(Colono colono) {
        colonosAsignados.add(colono);
    }

    public void desasignarColono(Colono colono) {
        colonosAsignados.remove(colono);
    }

    public void producirRecursos(int[] recursos) {
        switch(tipo) {
            case "Granja":
                // La Granja produce comida. Más colonos, más comida.
                recursos[1] += 5 * colonosAsignados.size();
                break;
            case "Central Energética":
                // La Central Energética produce energía.
                recursos[0] += 10 * colonosAsignados.size();
                break;
            case "Fábrica":
                // La Fábrica produce materiales.
                recursos[2] += 8 * colonosAsignados.size();
                break;
            default:
                System.out.println("Tipo de edificio desconocido.");
                break;
        }
    }

    public int obtenerNumeroDeColonos() {
        return colonosAsignados.size();
    }

    public void mostrarInformacion() {
        System.out.println("Edificio: " + nombre + " | Tipo: " + tipo + " | Colonos asignados: " + colonosAsignados.size());
    }
    
    // Método para calcular el impacto de la eficiencia de los colonos en la producción
    public void mejorarEficiencia(int[] recursos) {
        for (Colono colono : colonosAsignados) {
            if (colono.getHabilidad().equals(tipo)) {
                switch(tipo) {
                    case "Granja":
                        recursos[1] += 2; // Mejora en la producción de comida
                        break;
                    case "Central Energética":
                        recursos[0] += 4; // Mejora en la producción de energía
                        break;
                    case "Fábrica":
                        recursos[2] += 3; // Mejora en la producción de materiales
                        break;
                }
            }
        }
    }
}
