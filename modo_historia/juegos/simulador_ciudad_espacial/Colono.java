package modo_historia.juegos.simulador_ciudad_espacial;

public class Colono {
    private String nombre;
    private String habilidad; // Habilidad principal del colono, por ejemplo: "Agricultor", "Ingeniero", etc.
    private boolean asignado; // Indica si el colono está asignado a un edificio

    public Colono(String nombre, String habilidad) {
        this.nombre = nombre;
        this.habilidad = habilidad;
        this.asignado = false; // Inicialmente, el colono no está asignado a ningún edificio
    }

    public String getNombre() {
        return nombre;
    }

    public String getHabilidad() {
        return habilidad;
    }

    public boolean isAsignado() {
        return asignado;
    }

    public void setAsignado(boolean asignado) {
        this.asignado = asignado;
    }

    public void mostrarInformacion() {
        System.out.println("Colono: " + nombre + " | Habilidad: " + habilidad + " | Asignado: " + (asignado ? "Sí" : "No"));
    }
    
    public void trabajar(Edificio edificio) {
        if (!asignado) {
            edificio.asignarColono(this);
            this.asignado = true;
            System.out.println(nombre + " ha sido asignado al edificio: " + edificio.getNombre());
        } else {
            System.out.println(nombre + " ya está asignado a un edificio.");
        }
    }

    public void desasignar() {
        if (asignado) {
            this.asignado = false;
            System.out.println(nombre + " ha sido desasignado del edificio.");
        } else {
            System.out.println(nombre + " no está asignado a ningún edificio.");
        }
    }
}
