# Simulador de la ciudad espacial Skillcity

## Objetivo

El objetivo de este proyecto es desarrollar un simulador de gestión de una ciudad espacial utilizando arrays, arrays multidimensionales y ArrayLists en Java. Se deberá diseñar y programar una simulación en la que administre una ciudad espacial en un planeta lejano. La tarea principal es mantener el equilibrio entre la construcción de edificios, la asignación de colonos y la gestión de recursos para que la ciudad prospere durante un número determinado de turnos.

## Descripción del funcionamiento del programa

El programa consiste en simular la administración de una ciudad espacial representada en una cuadrícula (mapa) de 10x10. Se podrá construir diferentes tipos de edificios en las celdas de esta cuadrícula, cada uno con un propósito específico, como la producción de energía, alimentos o materiales.

Además, la ciudad contará con colonos que podrán ser asignados a diferentes edificios según sus habilidades, lo que afectará la producción y el consumo de recursos en cada turno. El programa deberá controlar el estado de la ciudad turno a turno, verificando la cantidad de recursos disponibles y realizando ajustes según las decisiones del usuario.

El juego debe permitir:

- Construcción de Edificios: El estudiante puede construir edificios en cualquier celda vacía del mapa, utilizando los recursos disponibles.
- Asignación de Colonos: Los colonos pueden ser asignados a edificios para aumentar la eficiencia de la producción.
- Gestión de Recursos: Los recursos como energía, comida y materiales deben ser gestionados cuidadosamente para evitar que la ciudad colapse.
- Simulación de Turnos: Cada turno, los edificios producen o consumen recursos según la cantidad y el tipo de colonos asignados.
- El juego finaliza después de un número predeterminado de turnos o si la ciudad se queda sin recursos esenciales.

## Funciones

### Inicializar Ciudad

Esta función deberá crear el mapa de la ciudad (un array bidimensional de 10x10) y establecer los recursos iniciales disponibles (energía, comida, materiales).

### Construir Edificio

Permite al estudiante construir un edificio en una posición específica del mapa, si los recursos necesarios están disponibles. Deberá actualizar el mapa, el ArrayList de edificios y los recursos disponibles.

### Asignar Colono

Esta función asigna un colono a un edificio específico. Deberá actualizar el ArrayList de colonos y la eficiencia del edificio.

### Pasar Turno

Simula un turno en el juego, donde todos los edificios producen o consumen recursos según los colonos asignados. Actualiza el estado de los recursos y verifica si la ciudad puede continuar funcionando.

### Mostrar Mapa

Muestra el estado actual del mapa de la ciudad, indicando dónde están ubicados los edificios y cuáles son las celdas vacías.

### Mostrar Estado de Recursos

Muestra la cantidad actual de recursos disponibles (energía, comida, materiales).

### Evento Aleatorio

Genera un evento aleatorio que puede afectar a la ciudad, como una tormenta espacial que daña edificios o una llegada de nuevos colonos que se suman a la población.

### Finalizar Juego

Determina si se ha alcanzado la condición de victoria (la ciudad ha sobrevivido el número necesario de turnos) o de derrota (los recursos se han agotado).