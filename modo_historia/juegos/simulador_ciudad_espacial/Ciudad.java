package modo_historia.juegos.simulador_ciudad_espacial;

import java.util.ArrayList;

public class Ciudad {
    private String[][] mapa = new String[10][10];
    private ArrayList<Edificio> edificios = new ArrayList<>();
    private ArrayList<Colono> colonos = new ArrayList<>();
    private int[] recursos = new int[3]; // 0: Energía, 1: Comida, 2: Materiales

    public Ciudad() {
        // Inicialización del mapa y recursos
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                mapa[i][j] = "Vacio";
            }
        }
        recursos[0] = 100; // Energía inicial
        recursos[1] = 100; // Comida inicial
        recursos[2] = 100; // Materiales iniciales
    }

    public void construirEdificio(int x, int y, Edificio edificio) {
        if (mapa[x][y].equals("Vacio") && recursos[2] >= edificio.getCosteMateriales()) {
            mapa[x][y] = edificio.getNombre();
            edificios.add(edificio);
            recursos[2] -= edificio.getCosteMateriales();
        } else {
            System.out.println("No se puede construir en esa ubicación o no hay suficientes materiales.");
        }
    }

    public void asignarColonoAEdificio(int colonoIndex, int edificioIndex) {
        if (colonoIndex < colonos.size() && edificioIndex < edificios.size()) {
            Colono colono = colonos.get(colonoIndex);
            Edificio edificio = edificios.get(edificioIndex);
            edificio.asignarColono(colono);
        } else {
            System.out.println("Índice de colono o edificio inválido.");
        }
    }

    public void pasarTurno() {
        for (Edificio edificio : edificios) {
            edificio.producirRecursos(recursos);
        }
        System.out.println("Turno completado.");
        mostrarEstadoRecursos();
    }

    public void mostrarMapa() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(mapa[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public void mostrarEstadoRecursos() {
        System.out.println("Energía: " + recursos[0]);
        System.out.println("Comida: " + recursos[1]);
        System.out.println("Materiales: " + recursos[2]);
    }

    public void agregarColono(Colono colono) {
        colonos.add(colono);
    }

    // Método para obtener un colono por su índice
    public Colono getColono(int index) {
        if (index >= 0 && index < colonos.size()) {
            return colonos.get(index);
        } else {
            System.out.println("Índice de colono inválido.");
            return null;
        }
    }

    public void mostrarInformacionColonos() {
        if (colonos.isEmpty()) {
            System.out.println("No hay colonos en la ciudad.");
        } else {
            for (int i = 0; i < colonos.size(); i++) {
                Colono colono = colonos.get(i);
                System.out.print("Índice: " + i + " | ");
                colono.mostrarInformacion();
            }
        }
    }

    // Otros métodos adicionales que se pueden agregar
}
