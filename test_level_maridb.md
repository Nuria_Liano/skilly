# Nivelador de MariaDB/Mysql

## 1. Crea una base de datos llamada empresa.

## 2. Crea una tabla empleados en la base de datos empresa con los campos: id (INT, AUTO_INCREMENT, PRIMARY KEY), nombre (VARCHAR(100)), apellidos (VARCHAR(100)), salario (DECIMAL(10,2)) y fecha_contratacion (DATE).

## 3. Inserta tres empleados en la tabla empleados.

## 4. Realiza una consulta que muestre el nombre y apellido de los empleados que tienen un salario mayor a 2000.

## 5. Actualiza el salario de un empleado cuyo id es 1, incrementándolo en un 10%.

## 6. Elimina de la tabla empleados al empleado cuyo id es 3.

## 7. Crea una nueva tabla departamentos con los campos: id_departamento (INT, PRIMARY KEY), nombre_departamento (VARCHAR(50)).

## 8. Inserta dos departamentos en la tabla departamentos.

## 9. Realiza una consulta que muestre todos los empleados junto con el nombre de su departamento (usa un JOIN entre empleados y departamentos).

## 10. Realiza una copia de seguridad de la base de datos empresa utilizando una instrucción de MariaDB.