# Trucos en HTML y Cuándo Usar Etiquetas Específicas

## 1. Listas (`<ul>`, `<ol>`, `<li>`) vs. Tablas (`<table>`, `<tr>`, `<td>`)

- **Listas** se utilizan cuando tienes un conjunto de elementos que son conceptualmente similares o que necesitan ser presentados en un orden secuencial o no secuencial.
  - **Usa listas cuando:** Quieras presentar un grupo de ítems relacionados como una lista de tareas, un menú de navegación, o una serie de pasos.
    - **Ejemplo:** Una lista de tareas pendientes en una aplicación de tareas.

    ```html
    <ul>
        <li>Comprar leche</li>
        <li>Hacer ejercicio</li>
        <li>Leer un libro</li>
    </ul>
    ```

- **Tablas** son ideales para mostrar datos tabulares, donde tienes relaciones claras entre filas y columnas, como horarios, resultados de exámenes, o precios de productos.
  - **Usa tablas cuando:** Necesites organizar datos en filas y columnas para facilitar la comparación.
    - **Ejemplo:** Una tabla con los horarios de clases.

    ```html
    <table>
        <tr>
            <th>Hora</th>
            <th>Lunes</th>
            <th>Martes</th>
            <th>Miércoles</th>
        </tr>
        <tr>
            <td>08:00</td>
            <td>Matemáticas</td>
            <td>Ciencias</td>
            <td>Historia</td>
        </tr>
        <tr>
            <td>09:00</td>
            <td>Inglés</td>
            <td>Deporte</td>
            <td>Arte</td>
        </tr>
    </table>
    ```

## 2. `<div>` vs. `<section>`

- **`<div>`** es un contenedor genérico sin significado semántico específico. Se utiliza principalmente para agrupar elementos y aplicarles estilos o scripts. 
  - **Usa `<div>` cuando:** No necesites agregar semántica adicional al contenido y solo quieres agrupar elementos por razones de diseño o comportamiento.

    ```html
    <div class="container">
        <div class="item">Contenido 1</div>
        <div class="item">Contenido 2</div>
    </div>
    ```

- **`<section>`** tiene un propósito semántico más claro, ya que indica que el contenido dentro de la etiqueta forma parte de una sección lógica del documento.
  - **Usa `<section>` cuando:** Estés agrupando contenido temáticamente relacionado, como una sección de noticias, un bloque de artículos, o una parte de una página web.
  
    ```html
    <section>
        <h2>Noticias Recientes</h2>
        <p>Aquí puedes ver las últimas noticias...</p>
    </section>
    ```

## 3. `<span>` vs. `<strong>` y `<em>`

- **`<span>`** es un contenedor en línea que, al igual que `<div>`, no tiene significado semántico, pero se utiliza para aplicar estilos o scripts a un segmento de texto.
  - **Usa `<span>` cuando:** Necesites aplicar estilos a una parte específica de un texto sin alterar su significado.

    ```html
    <p>Este es un <span class="resaltado">texto resaltado</span> dentro de un párrafo.</p>
    ```

- **`<strong>`** y **`<em>`** son etiquetas semánticas que indican importancia y énfasis respectivamente.
  - **Usa `<strong>` cuando:** Quieras destacar que un texto es de vital importancia, generalmente se renderiza en negrita.
  
    ```html
    <p>Recuerda: <strong>No compartas tus contraseñas.</strong></p>
    ```

  - **Usa `<em>` cuando:** Quieras poner énfasis en una palabra o frase, generalmente se renderiza en cursiva.
  
    ```html
    <p>Este es un punto <em>muy importante</em> a considerar.</p>
    ```

## 4. `<a>` con `href="#"` vs. `href="URL"`

- **`<a>`** se utiliza para crear enlaces. Dependiendo del valor del atributo `href`, el enlace puede dirigir al usuario a diferentes lugares.
  - **Usa `href="#"` cuando:** Quieras crear un enlace que no navegue a otra página pero que pueda estar vinculado a una acción JavaScript, como mostrar un modal o realizar un scroll en la página.

    ```html
    <a href="#" onclick="mostrarModal()">Haz clic aquí para más información</a>
    ```

  - **Usa `href="URL"` cuando:** Quieras que el enlace dirija a otra página o recurso en la web.
  
    ```html
    <a href="https://www.ejemplo.com">Visita nuestro sitio web</a>
    ```

## 5. Imágenes como `background-image` en CSS vs. `<img>` en HTML

- **`<img>`** se utiliza para mostrar imágenes que son parte integral del contenido de la página, como fotos, gráficos o ilustraciones relevantes para el contexto.
  - **Usa `<img>` cuando:** La imagen es un contenido visual que los usuarios deben ver para comprender el contexto.

    ```html
    <img src="foto.jpg" alt="Descripción de la imagen">
    ```

- **`background-image`** en CSS se utiliza para establecer imágenes de fondo que son decorativas y no forman parte del contenido principal.
  - **Usa `background-image` cuando:** La imagen es decorativa y no necesita ser accesible o indexada por los motores de búsqueda.

    ```css
    .banner {
        background-image: url('fondo.jpg');
        height: 200px;
    }
    ```

## 6. `<header>` vs. `<h1>`

- **`<header>`** es una etiqueta semántica que agrupa el encabezado de un documento o una sección.
  - **Usa `<header>` cuando:** Necesites crear una cabecera que pueda contener varios elementos como un título (`<h1>`), un menú de navegación (`<nav>`), y logotipos.

    ```html
    <header>
        <h1>Mi Sitio Web</h1>
        <nav>
            <ul>
                <li><a href="#inicio">Inicio</a></li>
                <li><a href="#sobre">Sobre mí</a></li>
            </ul>
        </nav>
    </header>
    ```

- **`<h1>`** es una etiqueta que define el título principal del documento o una sección.
  - **Usa `<h1>` cuando:** Necesites definir el título principal de una página o sección. Cada página debe tener solo un `<h1>`.

    ```html
    <h1>Bienvenidos a Mi Blog</h1>
    ```

## 7. `const` vs. `let`

- **`const`** se utiliza para declarar variables que no cambiarán su valor una vez asignado.
  - **Usa `const` cuando:** Estés seguro de que el valor de la variable no debe cambiar durante la ejecución del programa.

    ```javascript
    const PI = 3.14159;
    ```

- **`let`** se utiliza para declarar variables que pueden cambiar su valor.
  - **Usa `let` cuando:** Necesites una variable cuyo valor puede cambiar a lo largo del programa.

    ```javascript
    let contador = 0;
    contador++;
    ```

## 8. Uso de Parámetros en Funciones

- Los **parámetros** en funciones permiten pasar información a una función al momento de llamarla.
  - **Usa parámetros cuando:** Quieras que una función opere de manera flexible dependiendo de los valores que se le pasen.

    ```javascript
    function saludar(nombre) {
        return `Hola, ${nombre}`;
    }

    console.log(saludar('Carlos')); // "Hola, Carlos"
    ```

## 9. `checkbox` vs. `radio`

- **`<input type="checkbox">`** permite seleccionar múltiples opciones de un conjunto.
  - **Usa `checkbox` cuando:** Quieras permitir al usuario seleccionar más de una opción.

    ```html
    <label><input type="checkbox" name="intereses" value="deportes"> Deportes</label>
    <label><input type="checkbox" name="intereses" value="musica"> Música</label>
    ```

- **`<input type="radio">`** permite seleccionar una sola opción dentro de un conjunto.
  - **Usa `radio` cuando:** El usuario solo deba seleccionar una opción entre varias.

    ```html
    <label><input type="radio" name="genero" value="masculino"> Masculino</label>
    <label><input type="radio" name="genero" value="femenino"> Femenino</label>
    ```

## 10. `label`

- **`<label>`** se utiliza para asociar un texto con un control de formulario, mejorando la accesibilidad.
  - **Usa `label` cuando:** Necesites proporcionar una descripción clara para los campos de un formulario.

    ```html
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre">
    ```

## 11. `class` vs. `id`

- **`class`** se utiliza para aplicar estilos a múltiples elementos que comparten la misma clase.
  - **Usa `class` cuando:** Quieras aplicar los mismos estilos o comportamientos a varios elementos en una página.

    ```html
    <div class="tarjeta">Contenido 1</div>
    <div class="tarjeta">Contenido 2</div>
    ```

- **`id`** se utiliza para identificar un único elemento en la página. Cada `id` debe ser único dentro del documento.
  - **Usa `id` cuando:** Necesites seleccionar un único elemento para aplicarle estilos o manipularlo con JavaScript.

    ```html
    <div id="encabezado">Este es el encabezado principal</div>
    ```

