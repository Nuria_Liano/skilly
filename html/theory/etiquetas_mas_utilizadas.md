# Etiquetas de HTML más utilizadas

| Etiqueta     | Descripción                                                                 |
|--------------|-----------------------------------------------------------------------------|
| `<!DOCTYPE>` | Define el tipo de documento.                                                |
| `<html>`     | El elemento raíz que encierra todo el contenido HTML.                       |
| `<head>`     | Contiene metadatos sobre el documento.                                      |
| `<title>`    | Define el título del documento que aparece en la pestaña del navegador.     |
| `<meta>`     | Proporciona metadatos como el conjunto de caracteres o la descripción.      |
| `<link>`     | Vincula recursos externos, como hojas de estilo CSS.                        |
| `<style>`    | Define estilos CSS dentro del documento HTML.                               |
| `<script>`   | Incluye o enlaza scripts de JavaScript.                                     |
| `<body>`     | Contiene el contenido visible de la página web.                             |
| `<header>`   | Representa el encabezado de un documento o una sección.                     |
| `<nav>`      | Define una sección de navegación, como un menú.                             |
| `<main>`     | Contiene el contenido principal de la página.                               |
| `<section>`  | Define secciones dentro del documento.                                      |
| `<article>`  | Representa un contenido independiente, como un artículo.                   |
| `<aside>`    | Contiene contenido relacionado de manera secundaria.                       |
| `<footer>`   | Representa el pie de página del documento o de una sección.                 |
| `<h1>` a `<h6>`| Define encabezados, siendo `<h1>` el de mayor importancia.               |
| `<p>`        | Define un párrafo.                                                          |
| `<a>`        | Crea un enlace.                                                             |
| `<img>`      | Inserta una imagen.                                                         |
| `<ul>`       | Define una lista desordenada.                                               |
| `<ol>`       | Define una lista ordenada.                                                  |
| `<li>`       | Define un ítem de una lista.                                                |
| `<div>`      | Define un contenedor genérico.                                              |
| `<span>`     | Define un contenedor en línea.                                              |
| `<strong>`   | Indica un texto de gran importancia (negrita).                              |
| `<em>`       | Indica énfasis (cursiva).                                                   |
| `<form>`     | Define un formulario.                                                       |
| `<input>`    | Define un campo de entrada dentro de un formulario.                         |
| `<label>`    | Etiqueta un campo de formulario.                                            |
| `<button>`   | Crea un botón interactivo.                                                  |
| `<table>`    | Define una tabla.                                                           |
| `<tr>`       | Define una fila de tabla.                                                   |
| `<td>`       | Define una celda de tabla.                                                  |
| `<th>`       | Define un encabezado de tabla.                                              |


## Ejemplo de uso

~~~html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Página de ejemplo utilizando etiquetas HTML y etiquetas semánticas.">
    <title>Página de Ejemplo</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Bienvenidos a Mi Página de Ejemplo</h1>
        <nav>
            <ul>
                <li><a href="#inicio">Inicio</a></li>
                <li><a href="#articulo">Artículo</a></li>
                <li><a href="#contacto">Contacto</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <section id="inicio">
            <h2>Sobre esta Página</h2>
            <p>Esta es una página de ejemplo diseñada para mostrar el uso de etiquetas HTML, incluyendo las semánticas.</p>
            <img src="imagen.jpg" alt="Descripción de la imagen">
        </section>

        <article id="articulo">
            <h2>Un Artículo de Ejemplo</h2>
            <p>Lorem ipsum dolor sit amet, <strong>consectetur adipiscing</strong> elit. <em>Suspendisse</em> nec urna vitae augue venenatis.</p>
            <p>Si deseas leer más, visita <a href="https://www.ejemplo.com">este enlace</a>.</p>
        </article>

        <aside>
            <h3>Información Adicional</h3>
            <p>Este es un contenido relacionado que no forma parte del contenido principal.</p>
        </aside>
    </main>

    <footer>
        <section id="contacto">
            <h2>Contacto</h2>
            <form action="submit_form.php" method="post">
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="nombre" required>
                <label for="email">Correo Electrónico:</label>
                <input type="email" id="email" name="email" required>
                <button type="submit">Enviar</button>
            </form>
        </section>
        <p>&copy; 2024 Mi Página de Ejemplo. Todos los derechos reservados.</p>
    </footer>
</body>
</html>

~~~