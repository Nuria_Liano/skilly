# Ejercicios de Conversión entre Sistemas Numéricos

## 1. Decimal a Binario, Octal y Hexadecimal
Convierte los siguientes números decimales a binario, octal y hexadecimal:
- 45
- 128
- 255
- 512

---

## 2. Binario a Decimal, Octal y Hexadecimal
Convierte los siguientes números binarios a decimal, octal y hexadecimal:
- 1101
- 101010
- 11111111
- 100000000

---

## 3. Octal a Decimal, Binario y Hexadecimal
Convierte los siguientes números octales a decimal, binario y hexadecimal:
- 25
- 67
- 377
- 1000

---

## 4. Hexadecimal a Decimal, Binario y Octal
Convierte los siguientes números hexadecimales a decimal, binario y octal:
- A
- 1F
- 7E
- 2B7

---

## 5. Mixto: Combinación de sistemas
Realiza las siguientes conversiones entre sistemas numéricos:
- Convierte 100101 (binario) a octal y hexadecimal.  
- Convierte 52 (octal) a binario y decimal.  
- Convierte 3C (hexadecimal) a binario y decimal.  
- Convierte 156 (decimal) a binario, octal y hexadecimal.

---

## 6. Problemas prácticos
Responde las siguientes preguntas basadas en conversiones:
- Si tienes un número binario 1110, ¿cuántos bits necesitas para representarlo en hexadecimal?
- Convierte 200 (decimal) a binario y verifica cuántos bits necesitas.
- Un sistema tiene una dirección 1A7 en hexadecimal. ¿Cuál es su valor en decimal y binario?

---

## 7. Reto avanzado: Operaciones entre sistemas
Realiza las siguientes operaciones y da el resultado en los sistemas indicados:
- Suma 1010 (binario) y 1001 (binario). Da el resultado en decimal y hexadecimal.  
- Resta 37 (decimal) y 15 (decimal). Da el resultado en binario y octal.  
- Multiplica 12 (octal) por 4 (decimal). Da el resultado en hexadecimal.

---

## 8. Desafío de interpretación
Un dispositivo solo acepta números en hexadecimal. Si tienes el número 745 en decimal:
- Convierte este número a hexadecimal para que sea aceptado.
- Explica cómo este número sería interpretado en binario y octal.

---

## 9. Errores comunes: Encuentra el fallo
Encuentra y corrige los errores en las siguientes conversiones:
- 45 (decimal) = 1001001 (binario).  
- 7F (hexadecimal) = 177 (octal).  
- 11011 (binario) = 34 (decimal).  
- 123 (octal) = 7B (hexadecimal).

---

## 10. Proyectos creativos: Tabla de conversiones
Crea una tabla que contenga los números del 0 al 15 en sus cuatro representaciones: decimal, binario, octal y hexadecimal.
3