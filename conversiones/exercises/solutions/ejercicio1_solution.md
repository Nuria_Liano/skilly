# Ejercicios de Conversión entre Sistemas Numéricos con Soluciones

## 1. Decimal a Binario, Octal y Hexadecimal
Convierte los siguientes números decimales a binario, octal y hexadecimal:

- **45**
  - Binario: 101101
  - Octal: 55
  - Hexadecimal: 2D

- **128**
  - Binario: 10000000
  - Octal: 200
  - Hexadecimal: 80

- **255**
  - Binario: 11111111
  - Octal: 377
  - Hexadecimal: FF

- **512**
  - Binario: 1000000000
  - Octal: 1000
  - Hexadecimal: 200

---

## 2. Binario a Decimal, Octal y Hexadecimal
Convierte los siguientes números binarios a decimal, octal y hexadecimal:

- **1101**
  - Decimal: 13
  - Octal: 15
  - Hexadecimal: D

- **101010**
  - Decimal: 42
  - Octal: 52
  - Hexadecimal: 2A

- **11111111**
  - Decimal: 255
  - Octal: 377
  - Hexadecimal: FF

- **100000000**
  - Decimal: 256
  - Octal: 400
  - Hexadecimal: 100

---

## 3. Octal a Decimal, Binario y Hexadecimal
Convierte los siguientes números octales a decimal, binario y hexadecimal:

- **25**
  - Decimal: 21
  - Binario: 10101
  - Hexadecimal: 15

- **67**
  - Decimal: 55
  - Binario: 110111
  - Hexadecimal: 37

- **377**
  - Decimal: 255
  - Binario: 11111111
  - Hexadecimal: FF

- **1000**
  - Decimal: 512
  - Binario: 1000000000
  - Hexadecimal: 200

---

## 4. Hexadecimal a Decimal, Binario y Octal
Convierte los siguientes números hexadecimales a decimal, binario y octal:

- **A**
  - Decimal: 10
  - Binario: 1010
  - Octal: 12

- **1F**
  - Decimal: 31
  - Binario: 11111
  - Octal: 37

- **7E**
  - Decimal: 126
  - Binario: 1111110
  - Octal: 176

- **2B7**
  - Decimal: 695
  - Binario: 1010110111
  - Octal: 1267

---

## 5. Mixto: Combinación de sistemas
Realiza las siguientes conversiones entre sistemas numéricos:

- **100101 (binario) a octal y hexadecimal**
  - Octal: 45
  - Hexadecimal: 25

- **52 (octal) a binario y decimal**
  - Binario: 101010
  - Decimal: 42

- **3C (hexadecimal) a binario y decimal**
  - Binario: 111100
  - Decimal: 60

- **156 (decimal) a binario, octal y hexadecimal**
  - Binario: 10011100
  - Octal: 234
  - Hexadecimal: 9C

---

## 6. Problemas prácticos

1. **Si tienes un número binario 1110, ¿cuántos bits necesitas para representarlo en hexadecimal?**
   - Se necesitan 4 bits, ya que cada dígito hexadecimal representa exactamente 4 bits.

2. **Convierte 200 (decimal) a binario y verifica cuántos bits necesitas.**
   - Binario: 11001000
   - Bits necesarios: 8

3. **Un sistema tiene una dirección 1A7 en hexadecimal. ¿Cuál es su valor en decimal y binario?**
   - Decimal: 423
   - Binario: 110100111

---

## 7. Reto avanzado: Operaciones entre sistemas

1. **Suma 1010 (binario) y 1001 (binario). Da el resultado en decimal y hexadecimal.**
   - Suma binaria: 10011
   - Decimal: 19
   - Hexadecimal: 13

2. **Resta 37 (decimal) y 15 (decimal). Da el resultado en binario y octal.**
   - Resta decimal: 22
   - Binario: 10110
   - Octal: 26

3. **Multiplica 12 (octal) por 4 (decimal). Da el resultado en hexadecimal.**
   - Octal 12 = Decimal 10
   - Multiplicación: 10 × 4 = 40
   - Hexadecimal: 28

---

## 8. Desafío de interpretación

1. **Un dispositivo solo acepta números en hexadecimal. Si tienes el número 745 en decimal:**
   - Hexadecimal: 2E9
   - Binario: 1011101001
   - Octal: 1351

---

## 9. Errores comunes: Encuentra el fallo

1. **45 (decimal) = 1001001 (binario).**  
   - Corrección: 45 (decimal) = 101101 (binario)

2. **7F (hexadecimal) = 177 (octal).**  
   - Corrección: 7F (hexadecimal) = 377 (octal)

3. **11011 (binario) = 34 (decimal).**  
   - Corrección: 11011 (binario) = 27 (decimal)

4. **123 (octal) = 7B (hexadecimal).**  
   - Corrección: 123 (octal) = 53 (hexadecimal)

---

## 10. Proyectos creativos: Tabla de conversiones
Crea una tabla que contenga los números del 0 al 15 en sus cuatro representaciones: decimal, binario, octal y hexadecimal.

| Decimal | Binario  | Octal | Hexadecimal |
|---------|----------|-------|-------------|
| 0       | 0000     | 0     | 0           |
| 1       | 0001     | 1     | 1           |
| 2       | 0010     | 2     | 2           |
| 3       | 0011     | 3     | 3           |
| 4       | 0100     | 4     | 4           |
| 5       | 0101     | 5     | 5           |
| 6       | 0110     | 6     | 6           |
| 7       | 0111     | 7     | 7           |
| 8       | 1000     | 10    | 8           |
| 9       | 1001     | 11    | 9           |
| 10      | 1010     | 12    | A           |
| 11      | 1011     | 13    | B           |
| 12      | 1100     | 14    | C           |
| 13      | 1101     | 15    | D           |
| 14      | 1110     | 16    | E           |
| 15      | 1111     | 17    | F           |
