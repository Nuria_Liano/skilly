# Conversión entre Decimal, Binario, Octal y Hexadecimal

## 1. Conversión de Decimal a Binario, Octal y Hexadecimal

### 1.1. Decimal a Binario
1. Divide el número decimal entre 2.
2. Guarda el resto (0 o 1).
3. Repite el proceso con el cociente hasta que sea 0.
4. Escribe los restos en orden inverso.

**Ejemplo:** Convertir 25 a binario.  
- 25 ÷ 2 = 12, resto: 1  
- 12 ÷ 2 = 6, resto: 0  
- 6 ÷ 2 = 3, resto: 0  
- 3 ÷ 2 = 1, resto: 1  
- 1 ÷ 2 = 0, resto: 1  

**Resultado:** 25 en decimal = 11001 en binario

---

### 1.2. Decimal a Octal
1. Divide el número decimal entre 8.
2. Guarda el resto.
3. Repite el proceso con el cociente hasta que sea 0.
4. Escribe los restos en orden inverso.

**Ejemplo:** Convertir 25 a octal.  
- 25 ÷ 8 = 3, resto: 1  
- 3 ÷ 8 = 0, resto: 3  

**Resultado:** 25 en decimal = 31 en octal

---

### 1.3. Decimal a Hexadecimal
1. Divide el número decimal entre 16.
2. Guarda el resto (usa dígitos 0–9 y letras A–F para restos mayores a 9).
3. Repite el proceso con el cociente hasta que sea 0.
4. Escribe los restos en orden inverso.

**Ejemplo:** Convertir 254 a hexadecimal.  
- 254 ÷ 16 = 15, resto: 14 (E)  
- 15 ÷ 16 = 0, resto: 15 (F)  

**Resultado:** 254 en decimal = FE en hexadecimal

---

## 2. Conversión de Binario a Decimal, Octal y Hexadecimal

### 2.1. Binario a Decimal
1. Multiplica cada dígito del binario por una potencia de 2, comenzando desde la derecha con 2^0.
2. Suma los resultados.

**Ejemplo:** Convertir 1011 en binario a decimal.  
- (1 × 2³) + (0 × 2²) + (1 × 2¹) + (1 × 2⁰)  
- 8 + 0 + 2 + 1 = 11  

**Resultado:** 1011 en binario = 11 en decimal

---

### 2.2. Binario a Octal
1. Agrupa los dígitos en grupos de 3 desde la derecha (añade ceros al inicio si es necesario).
2. Convierte cada grupo a su equivalente decimal.

**Ejemplo:** Convertir 101011 en binario a octal.  
- Agrupación: 101 y 011  
- 101 = 5, 011 = 3  

**Resultado:** 101011 en binario = 53 en octal

---

### 2.3. Binario a Hexadecimal
1. Agrupa los dígitos en grupos de 4 desde la derecha (añade ceros al inicio si es necesario).
2. Convierte cada grupo a su equivalente hexadecimal.

**Ejemplo:** Convertir 11010111 en binario a hexadecimal.  
- Agrupación: 1101 y 0111  
- 1101 = D, 0111 = 7  

**Resultado:** 11010111 en binario = D7 en hexadecimal

---

## 3. Conversión de Octal a Decimal, Binario y Hexadecimal

### 3.1. Octal a Decimal
1. Multiplica cada dígito por una potencia de 8, comenzando desde la derecha con 8^0.
2. Suma los resultados.

**Ejemplo:** Convertir 31 en octal a decimal.  
- (3 × 8¹) + (1 × 8⁰)  
- 24 + 1 = 25  

**Resultado:** 31 en octal = 25 en decimal

---

### 3.2. Octal a Binario
1. Convierte cada dígito del octal a su equivalente binario de 3 bits.

**Ejemplo:** Convertir 31 en octal a binario.  
- 3 = 011, 1 = 001  

**Resultado:** 31 en octal = 11001 en binario

---

### 3.3. Octal a Hexadecimal
1. Convierte el octal a binario (3 bits por dígito).
2. Agrupa los bits en bloques de 4.
3. Convierte los bloques a hexadecimal.

**Ejemplo:** Convertir 31 en octal a hexadecimal.  
- 31 en octal = 11001 en binario  
- Agrupación: 1100 y 0001 (completando ceros)  
- 1100 = C, 0001 = 1  

**Resultado:** 31 en octal = C1 en hexadecimal

---

## 4. Conversión de Hexadecimal a Decimal, Binario y Octal

### 4.1. Hexadecimal a Decimal
1. Multiplica cada dígito por una potencia de 16, comenzando desde la derecha con 16^0.
2. Suma los resultados.

**Ejemplo:** Convertir 1A en hexadecimal a decimal.  
- (1 × 16¹) + (10 × 16⁰)  
- 16 + 10 = 26  

**Resultado:** 1A en hexadecimal = 26 en decimal

---

### 4.2. Hexadecimal a Binario
1. Convierte cada dígito hexadecimal a su equivalente binario de 4 bits.

**Ejemplo:** Convertir 1A en hexadecimal a binario.  
- 1 = 0001, A = 1010  

**Resultado:** 1A en hexadecimal = 11010 en binario

---

### 4.3. Hexadecimal a Octal
1. Convierte el hexadecimal a binario (4 bits por dígito).
2. Agrupa los bits en bloques de 3.
3. Convierte cada bloque a su equivalente octal.

**Ejemplo:** Convertir 1A en hexadecimal a octal.  
- 1A en hexadecimal = 11010 en binario  
- Agrupación: 011 y 010  
- 011 = 3, 010 = 2  

**Resultado:** 1A en hexadecimal = 32 en octal
