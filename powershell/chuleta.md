# Chuleta de PowerShell

## Introducción a PowerShell
- **PowerShell** es una CLI (Command Line Interface) usada para la administración de sistemas en Windows.
- **Versión**: Integrado en Windows; se puede ejecutar desde "Inicio > Windows PowerShell".

## Comandos y Alias
- **Alias**: PowerShell puede usar comandos de CMD (como `dir`, `cd`) y comandos de Linux (como `ls`, `cp`) como alias.
- **Get-Alias**: Muestra todos los alias disponibles.
- **Get-Command**: Lista todos los comandos de PowerShell.
- **Get-Help**: Proporciona ayuda de un comando (`Get-Help <comando>`).

## Estructura Básica de Comandos
- Formato: `Verbo-Sustantivo` (ej. `Get-Help`, `Set-Date`).
- **Cmdlets comunes**:
  - `Get`: Obtiene datos.
  - `Set`: Establece o modifica datos.
  - `Format`: Aplica formato a datos.
  - `Out`: Dirige la salida a un destino.

## Variables y Tipos de Datos
- Declaración: `$variable = valor` (se ajusta automáticamente al tipo de dato).
- Tipos de datos:
  - `[int]`, `[double]`, `[string]`, `[bool]`, `[date]`, `[array]`, `[object]`.
- **Variables de entorno**: Accesibles usando `$Env:VARIABLENAME`.

## Operadores
### Aritméticos
- `+`, `-`, `*`, `/`, `%`: Suma, resta, multiplicación, división y módulo.

### Relacionales
- Números: `-eq` (igual), `-ne` (no igual), `-gt` (mayor), `-lt` (menor), `-ge` (mayor o igual), `-le` (menor o igual).
- Texto: `-like` (coincide con), `-notlike` (no coincide).
- Archivos/Directorios: `-a` (existe), `-r` (lectura), `-w` (escritura), `-x` (ejecución).

### Lógicos
- `-and`, `-or`, `-not`: Y, O, negación.

## Control de Flujo
### Condicionales
- **If-Else**:
  ```powershell
  if (<condición>) {
      # Código si la condición es verdadera
  } else {
      # Código si es falsa
  }
  ```
- **Switch**:
  ```powershell
  switch ($variable) {
    "valor1" { # Acción }
    "valor2" { # Acción }
    default { # Acción por defecto }
    }
  ```

### Estructuras de Iteración

- **For**:
  ```powershell
  for ($i = 0; $i -lt 10; $i++) {
    # Código a repetir
    }
  ```
- **While**:
  ```powershell
  while (<condición>) {
    # Código a repetir mientras la condición sea verdadera
    }
  ```
- **Foreach**:
  ```powershell
  foreach ($elemento in $colección) {
    # Código para cada elemento
    }
  ```

## Manipulación de Objetos

Select-Object: Selecciona propiedades específicas.
Where-Object: Filtra objetos basados en una condición (Where-Object {$_.Propiedad -eq "valor"}).
Measure-Object: Realiza cálculos (suma, promedio) sobre los valores de propiedad de los objetos.
Sort-Object: Ordena objetos en función de sus propiedades.

## Canalización (Pipes)

Conecta comandos, usando |, donde la salida de un comando sirve como entrada para el siguiente.

~~~ps
Get-Process | Where-Object {$_.CPU -gt 10} | Sort-Object CPU
~~~