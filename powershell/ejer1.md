# Examen de Scripting en PowerShell

**Instrucciones:** Crea un script en PowerShell que implemente un sistema básico de gestión de inventario para una tienda. Asegúrate de seguir los requisitos especificados para cada función y usa los conceptos de argumentos, variables, operadores, bucles y condicionales.

## Enunciado

Tu tarea es crear un script en PowerShell que permita a los usuarios administrar un inventario de productos en una tienda. El script debe aceptar un tipo de operación y argumentos adicionales, según el tipo de operación elegida.

### Requisitos

1. **Argumentos**: 
   - El script debe aceptar el tipo de operación como argumento (`add`, `remove`, `view`, `search`).
   - Si el usuario selecciona `add`, `remove` o `search`, se deben aceptar argumentos adicionales según la operación.

2. **Variables**:
   - Usa una variable para almacenar el inventario, que deberá ser una lista de productos. Cada producto debe incluir los siguientes datos:
     - `Nombre` (tipo cadena)
     - `Precio` (tipo numérico)

3. **Operadores**:
   - Usa operadores para realizar verificaciones, como comparar precios y verificar si un producto ya existe en el inventario antes de agregarlo o eliminarlo.

4. **Bucles**:
   - Implementa bucles para recorrer el inventario cuando sea necesario, como para visualizar o buscar productos específicos.

5. **Condicionales**:
   - Usa condicionales para determinar qué acción tomar según el tipo de operación seleccionada.

### Funcionalidades que debe incluir el script:

1. **Agregar un producto**:
   - Si el usuario pasa `add` como operación, el script debe agregar un nuevo producto al inventario con el nombre y precio proporcionados como argumentos.
   - Antes de agregar el producto, verifica que no exista otro producto con el mismo nombre en el inventario. Si ya existe, muestra un mensaje de error.

2. **Eliminar un producto**:
   - Si el usuario pasa `remove` como operación, el script debe eliminar el producto que coincida con el nombre proporcionado.
   - Si el producto no se encuentra en el inventario, muestra un mensaje de error.

3. **Ver el inventario**:
   - Si el usuario pasa `view` como operación, el script debe mostrar la lista completa de productos en el inventario, incluyendo el nombre y el precio de cada uno.

4. **Buscar productos por precio**:
   - Si el usuario pasa `search` como operación, el script debe mostrar todos los productos con un precio menor o igual al precio especificado.
   - Si no hay productos que coincidan con este criterio, muestra un mensaje indicando que no hay productos disponibles en ese rango de precio.

## Solucion

~~~ps
# Examen de Scripting en PowerShell - Solución Simplificada

# Parámetros que recibe el script
param (
    [string]$operation,     # Operación: "add", "remove", "view", o "search"
    [string]$productName,   # Nombre del producto (para "add" y "remove")
    [double]$price          # Precio del producto (para "add" y "search")
)

# Lista de productos (simulando un inventario inicial)
$inventory = @(
    @{Nombre = "Producto1"; Precio = 10.0},
    @{Nombre = "Producto2"; Precio = 20.0},
    @{Nombre = "Producto3"; Precio = 30.0}
)

# Función para agregar un producto al inventario
function Add-Product {
    param ([string]$name, [double]$price)
    $product = @{Nombre = $name; Precio = $price}
    $inventory += $product
    Write-Output "Producto agregado: $name - Precio: $price"
}

# Función para eliminar un producto del inventario
function Remove-Product {
    param ([string]$name)
    $inventory = $inventory | Where-Object { $_.Nombre -ne $name }
    Write-Output "Producto eliminado: $name"
}

# Función para mostrar el inventario completo
function View-Inventory {
    Write-Output "Inventario de productos:"
    foreach ($product in $inventory) {
        Write-Output "$($product.Nombre) - Precio: $($product.Precio)"
    }
}

# Función para buscar productos con precio igual o menor al especificado
function Search-ByPrice {
    param ([double]$maxPrice)
    Write-Output "Productos con precio menor o igual a $maxPrice:"
    foreach ($product in $inventory) {
        if ($product.Precio -le $maxPrice) {
            Write-Output "$($product.Nombre) - Precio: $($product.Precio)"
        }
    }
}

# Lógica principal para decidir qué operación ejecutar
switch ($operation) {
    "add" { 
        Add-Product -name $productName -price $price 
    }
    "remove" { 
        Remove-Product -name $productName 
    }
    "view" { 
        View-Inventory 
    }
    "search" { 
        Search-ByPrice -maxPrice $price 
    }
    default { 
        Write-Output "Operación no válida. Usa 'add', 'remove', 'view' o 'search'."
    }
}

~~~
