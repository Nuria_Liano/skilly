# Tipos de `for` en Python

## 1. `for` con `range()`
El bucle `for` con `range()` se utiliza para iterar un número específico de veces. `range()` genera una secuencia de números enteros, y puedes controlar el inicio, el final y el paso de la secuencia.

**Ejemplo básico**:

~~~py
for i in range(5):
    print(i)

~~~

---

**Parámetros en `range()`**:
- **`range(stop)`**: Empieza desde 0 y termina en `stop - 1`.
- **`range(start, stop)`**: Empieza desde `start` y termina en `stop - 1`.
- **`range(start, stop, step)`**: Empieza desde `start`, termina en `stop - 1`, y se incrementa por `step`.

**Ejemplo con `start` y `step`**:

~~~py
for i in range(1, 10, 2):
    print(i)
~~~

---

## 2. `for` con `len()` y listas o cadenas
Cuando necesitas iterar sobre una lista o cadena de texto y acceder a los índices de cada elemento, puedes usar `range()` combinado con `len()`.

**Ejemplo**:

~~~py
for i in range(1, 10, 2):
    print(i)
~~~

---

## 3. `for` directamente sobre listas, cadenas o cualquier iterable
Python permite iterar directamente sobre los elementos de una lista, cadena o cualquier otra estructura iterable sin la necesidad de usar índices.

**Ejemplo con lista**:

~~~py
frutas = ['manzana', 'banana', 'cereza']
for fruta in frutas:
    print(fruta)
~~~

---

**Ejemplo con cadena**:

~~~py
palabra = "Python"
for letra in palabra:
    print(letra)
~~~

---

## 4. `for` con diccionarios
Los diccionarios en Python tienen métodos que te permiten iterar sobre las claves, los valores o ambos (pares clave-valor).

**Iterar sobre las claves**:

~~~py
diccionario = {'nombre': 'Juan', 'edad': 30, 'ciudad': 'Madrid'}
for clave in diccionario:
    print(clave)
~~~

---

**Iterar sobre las claves y valores**:

~~~py
for clave, valor in diccionario.items():
    print(clave, valor)
~~~

---

## 5. `for` con `enumerate()`
`enumerate()` es muy útil cuando quieres tanto los índices como los elementos al iterar sobre una lista u otro iterable.

**Ejemplo**:

~~~py
frutas = ['manzana', 'banana', 'cereza']
for i, fruta in enumerate(frutas):
    print(i, fruta)
~~~

---

## 6. `for` con `zip()`
`zip()` te permite iterar sobre dos o más iterables a la vez, emparejando sus elementos.

**Ejemplo**:

~~~py
nombres = ['Juan', 'Ana', 'Pedro']
edades = [30, 25, 22]

for nombre, edad in zip(nombres, edades):
    print(f'{nombre} tiene {edad} años')
~~~

---

## Resumen
- **`range()`**: Genera una secuencia de números enteros.
- **`len()`**: Devuelve la longitud de una lista, cadena u otra estructura.
- **Iterar sobre listas o cadenas**: Puedes iterar directamente sobre los elementos de una lista o cadena.
- **`enumerate()`**: Para obtener tanto los índices como los elementos.
- **`zip()`**: Para iterar sobre múltiples listas simultáneamente.
- **Diccionarios**: Puedes iterar sobre claves, valores o pares clave-valor.
