# Proyecto: **"Sistema Solar Interactivo"**

## Enunciado
Crea un programa en Python que permita a los usuarios explorar un sistema solar ficticio, en el que podrán interactuar con planetas, lunas y estrellas. El programa debe simular el sistema solar utilizando clases, objetos, métodos, bucles y estructuras de control como `for`, `while` e `if`.

---

## **Estructura del proyecto**
El proyecto constará de las siguientes clases, cada una en un archivo independiente:

---

### **1. Clase `Estrella`**
Esta clase representa una estrella (por ejemplo, el Sol).

#### **Atributos**
- `nombre` (str): Nombre de la estrella.
- `temperatura` (float): Temperatura de la estrella en Kelvin.
- `tamaño` (float): Tamaño relativo de la estrella.
- `planetas` (list): Lista de objetos tipo `Planeta` orbitando la estrella.

#### **Métodos**
- `__init__`: Inicializa los atributos de la estrella.
- `añadir_planeta(planeta)`: Añade un objeto `Planeta` a la lista de planetas.
- `mostrar_informacion()`: Muestra el nombre, temperatura, tamaño y número de planetas orbitando.
- `listar_planetas()`: Recorre la lista de planetas y muestra sus nombres.

---

### **2. Clase `Planeta`**
Esta clase representa un planeta.

#### **Atributos**
- `nombre` (str): Nombre del planeta.
- `distancia_a_estrella` (float): Distancia del planeta a la estrella en millones de kilómetros.
- `tamaño` (float): Tamaño relativo del planeta.
- `lunas` (list): Lista de objetos tipo `Luna` asociados al planeta.

#### **Métodos**
- `__init__`: Inicializa los atributos del planeta.
- `añadir_luna(luna)`: Añade un objeto `Luna` a la lista de lunas.
- `mostrar_informacion()`: Muestra el nombre, distancia a la estrella y el número de lunas.
- `listar_lunas()`: Muestra un listado de las lunas del planeta.

---

### **3. Clase `Luna`**
Esta clase representa una luna que orbita un planeta.

#### **Atributos**
- `nombre` (str): Nombre de la luna.
- `orbita` (str): Nombre del planeta al que orbita.
- `tamaño` (float): Tamaño relativo de la luna.

#### **Métodos**
- `__init__`: Inicializa los atributos de la luna.
- `mostrar_informacion()`: Muestra el nombre de la luna, el nombre del planeta al que orbita y su tamaño.

---

### **4. Clase `SistemaSolar`**
Esta clase gestiona el sistema solar completo.

#### **Atributos**
- `estrella` (objeto `Estrella`): La estrella central del sistema solar.
- `planetas` (list): Lista de planetas del sistema solar.

#### **Métodos**
- `__init__`: Inicializa el sistema solar con una estrella y sus planetas.
- `agregar_planeta(planeta)`: Agrega un objeto `Planeta` al sistema solar.
- `interactuar()`: Método principal que permite al usuario explorar el sistema solar mediante un bucle `while`. El usuario puede:
  - Ver información de la estrella.
  - Listar planetas.
  - Seleccionar un planeta para ver su información.
  - Ver lunas de un planeta seleccionado.
  - Salir del programa.

---

## **Requisitos del programa**
1. Implementa una simulación sencilla de un sistema solar ficticio con al menos:
   - 1 estrella.
   - 3 planetas (con al menos uno con lunas).
   - 2 lunas en total.
2. Permite que el usuario explore el sistema solar interactuando con el programa.
3. Organiza las clases en diferentes archivos y usa importaciones para conectarlas.
4. Usa bucles para listar planetas y lunas, y estructuras condicionales para manejar las elecciones del usuario.

---

## **Extensiones posibles**
- Añade asteroides o cinturones de asteroides como nuevos objetos.
- Agrega un sistema de búsqueda por nombre de planetas o lunas.
- Implementa un cálculo de distancias entre planetas.
