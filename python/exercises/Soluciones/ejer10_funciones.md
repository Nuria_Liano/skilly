# Practicando funciones

## 1. Calcular el Área de un Círculo
  
### Resultado

~~~py
import math

def calcular_area_circulo(radio):
    return math.pi * radio ** 2

def main():
    radio = float(input("Ingrese el radio del círculo: "))
    area = calcular_area_circulo(radio)
    print(f"El área del círculo es: {area:.2f}")

main()

~~~

## 2. Convertir Grados Celsius a Fahrenheit

### Resultado

~~~py
def celsius_a_fahrenheit(celsius):
    return (celsius * 9/5) + 32

def main():
    celsius = float(input("Ingrese la temperatura en grados Celsius: "))
    fahrenheit = celsius_a_fahrenheit(celsius)
    print(f"La temperatura en grados Fahrenheit es: {fahrenheit:.2f}")

main()

~~~

## 3. Contar Vocales en una Cadena

### Resultado

~~~py
def contar_vocales(cadena):
    vocales = "aeiouAEIOU"
    contador = 0
    for caracter in cadena:
        if caracter in vocales:
            contador += 1
    return contador

def main():
    cadena = input("Ingrese una cadena de texto: ")
    num_vocales = contar_vocales(cadena)
    print(f"El número de vocales en la cadena es: {num_vocales}")

main()

~~~

## 4. Calcular el Factorial de un Número

### Resultado

~~~py
def calcular_factorial(n):
    factorial = 1
    for i in range(1, n + 1):
        factorial *= i
    return factorial

def main():
    numero = int(input("Ingrese un número entero: "))
    factorial = calcular_factorial(numero)
    print(f"El factorial de {numero} es: {factorial}")

main()

~~~

## 5. Verificar si un Número es Primo

### Resultado

~~~py
def es_primo(n):
    if n <= 1:
        return False
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return True

def main():
    numero = int(input("Ingrese un número entero: "))
    if es_primo(numero):
        print(f"{numero} es un número primo.")
    else:
        print(f"{numero} no es un número primo.")

main()

~~~

## 6. Invertir una Cadena de Texto

### Resultado

~~~py
def invertir_cadena(cadena):
    return cadena[::-1]

def main():
    cadena = input("Ingrese una cadena de texto: ")
    cadena_invertida = invertir_cadena(cadena)
    print(f"La cadena invertida es: {cadena_invertida}")

main()

~~~

## 7. Encontrar el Máximo en una Lista de Números

### Resultado

~~~py
def encontrar_maximo(lista):
    maximo = lista[0]
    for numero in lista:
        if numero > maximo:
            maximo = numero
    return maximo

def main():
    lista = list(map(float, input("Ingrese una lista de números separados por espacios: ").split()))
    maximo = encontrar_maximo(lista)
    print(f"El número máximo en la lista es: {maximo}")

main()

~~~

## 8. Calcular la Media de una Lista de Números

### Resultado

~~~py
def calcular_media(lista):
    return sum(lista) / len(lista)

def main():
    lista = list(map(float, input("Ingrese una lista de números separados por espacios: ").split()))
    media = calcular_media(lista)
    print(f"La media de los números en la lista es: {media:.2f}")

main()

~~~

## 9. Verificar Palíndromo

### Resultado

~~~py
def es_palindromo(cadena):
    cadena = cadena.replace(" ", "").lower()
    return cadena == cadena[::-1]

def main():
    cadena = input("Ingrese una cadena de texto: ")
    if es_palindromo(cadena):
        print(f"'{cadena}' es un palíndromo.")
    else:
        print(f"'{cadena}' no es un palíndromo.")

main()

~~~

## 10. Contar Elementos en una Lista

### Resultado

~~~py
def contar_elementos(lista):
    return len(lista)

def main():
    lista = input("Ingrese una lista de elementos separados por espacios: ").split()
    num_elementos = contar_elementos(lista)
    print(f"El número de elementos en la lista es: {num_elementos}")

main()
~~~
