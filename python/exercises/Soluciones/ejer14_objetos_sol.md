# [SOLUCIÓN] Proyecto: **"Sistema Solar Interactivo"**

## estrella.py

~~~py
class Estrella:
    def __init__(self, nombre, temperatura, tamaño):
        self.nombre = nombre
        self.temperatura = temperatura
        self.tamaño = tamaño
        self.planetas = []

    def añadir_planeta(self, planeta):
        self.planetas.append(planeta)

    def mostrar_informacion(self):
        print(f"Estrella: {self.nombre}")
        print(f"Temperatura: {self.temperatura} K")
        print(f"Tamaño: {self.tamaño} veces el Sol")
        print(f"Planetas en órbita: {len(self.planetas)}")

    def listar_planetas(self):
        print("Planetas en órbita:")
        for planeta in self.planetas:
            print(f"- {planeta.nombre}")

~~~

## planeta.py

~~~py
class Planeta:
    def __init__(self, nombre, distancia_a_estrella, tamaño):
        self.nombre = nombre
        self.distancia_a_estrella = distancia_a_estrella
        self.tamaño = tamaño
        self.lunas = []

    def añadir_luna(self, luna):
        self.lunas.append(luna)

    def mostrar_informacion(self):
        print(f"Planeta: {self.nombre}")
        print(f"Distancia a la estrella: {self.distancia_a_estrella} millones de km")
        print(f"Tamaño: {self.tamaño} veces la Tierra")
        print(f"Número de lunas: {len(self.lunas)}")

    def listar_lunas(self):
        print(f"Lunas de {self.nombre}:")
        for luna in self.lunas:
            print(f"- {luna.nombre}")
~~~

## luna.py

~~~py
class Luna:
    def __init__(self, nombre, orbita, tamaño):
        self.nombre = nombre
        self.orbita = orbita
        self.tamaño = tamaño

    def mostrar_informacion(self):
        print(f"Luna: {self.nombre}")
        print(f"Órbita: {self.orbita}")
        print(f"Tamaño: {self.tamaño} veces la Luna de la Tierra")
~~~

## sistema_solar.py

~~~py
from estrella import Estrella
from planeta import Planeta
from luna import Luna

class SistemaSolar:
    def __init__(self, estrella):
        self.estrella = estrella
        self.planetas = []

    def agregar_planeta(self, planeta):
        self.planetas.append(planeta)
        self.estrella.añadir_planeta(planeta)

    def interactuar(self):
        while True:
            print("\n--- Sistema Solar Interactivo ---")
            print("1. Ver información de la estrella")
            print("2. Listar planetas")
            print("3. Seleccionar un planeta")
            print("4. Salir")
            opcion = input("Selecciona una opción: ")

            if opcion == "1":
                self.estrella.mostrar_informacion()

            elif opcion == "2":
                self.estrella.listar_planetas()

            elif opcion == "3":
                planeta_nombre = input("Introduce el nombre del planeta: ")
                planeta = next((p for p in self.planetas if p.nombre == planeta_nombre), None)
                if planeta:
                    planeta.mostrar_informacion()
                    print("1. Ver lunas")
                    print("2. Volver")
                    sub_opcion = input("Selecciona una opción: ")
                    if sub_opcion == "1":
                        planeta.listar_lunas()
                else:
                    print("Planeta no encontrado.")

            elif opcion == "4":
                print("Saliendo del sistema solar interactivo...")
                break

            else:
                print("Opción no válida.")
~~~

## main.py

~~~py
from sistema_solar import SistemaSolar
from estrella import Estrella
from planeta import Planeta
from luna import Luna

# Crear estrella
estrella = Estrella("Solus", 5778, 1.0)

# Crear planetas
planeta1 = Planeta("Mercurion", 58, 0.38)
planeta2 = Planeta("Terranova", 150, 1.0)
planeta3 = Planeta("Giganteus", 778, 11.2)

# Crear lunas
luna1 = Luna("Lunaris", "Terranova", 1.0)
luna2 = Luna("Io", "Giganteus", 0.25)

# Añadir lunas a planetas
planeta2.añadir_luna(luna1)
planeta3.añadir_luna(luna2)

# Crear sistema solar
sistema_solar = SistemaSolar(estrella)

# Añadir planetas al sistema solar
sistema_solar.agregar_planeta(planeta1)
sistema_solar.agregar_planeta(planeta2)
sistema_solar.agregar_planeta(planeta3)

# Interacción del usuario
sistema_solar.interactuar()
~~~
