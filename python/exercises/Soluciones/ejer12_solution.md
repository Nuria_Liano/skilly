# Ejercicios de objetos simples

## 1: Crear y acceder a atributos de un objeto

~~~sh
class Persona:
    def __init__(self, nombre, edad, ciudad):
        self.nombre = nombre
        self.edad = edad
        self.ciudad = ciudad

# Crear un objeto
persona1 = Persona("Ana", 30, "Madrid")

# Imprimir atributos
print(f"Nombre: {persona1.nombre}")
print(f"Edad: {persona1.edad}")
print(f"Ciudad: {persona1.ciudad}")

~~~

## 2: Método en una clase

Crea una clase llamada Coche con los atributos marca, modelo y color. Define un método llamado descripcion que devuelva una cadena con la descripción del coche en el formato: "Marca: <marca>, Modelo: <modelo>, Color: <color>". Crea un objeto de la clase e imprime su descripción

~~~sh
class Coche:
    def __init__(self, marca, modelo, color):
        self.marca = marca
        self.modelo = modelo
        self.color = color
    
    def descripcion(self):
        return f"Marca: {self.marca}, Modelo: {self.modelo}, Color: {self.color}"

# Crear un objeto
coche1 = Coche("Toyota", "Corolla", "Rojo")

# Imprimir descripción
print(coche1.descripcion())

~~~

## 3: Contador de objetos

Crea una clase llamada Producto con un atributo de clase llamado contador que se incremente cada vez que se cree un nuevo objeto de la clase. El constructor debe recibir un nombre para el producto y almacenarlo en un atributo de instancia. Imprime el número de productos creados después de crear tres objetos.

~~~sh
class Producto:
    contador = 0  # Atributo de clase
    
    def __init__(self, nombre):
        self.nombre = nombre
        Producto.contador += 1  # Incrementar contador al crear un objeto

# Crear objetos
producto1 = Producto("Manzana")
producto2 = Producto("Plátano")
producto3 = Producto("Naranja")

# Imprimir contador
print(f"Productos creados: {Producto.contador}")

~~~

## 4: Modificar atributos de un objeto

Crea una clase llamada Libro con los atributos titulo y autor. Añade un método actualizar_titulo que permita cambiar el título del libro. Crea un objeto, modifica su título e imprime los cambios.

~~~sh
class Libro:
    def __init__(self, titulo, autor):
        self.titulo = titulo
        self.autor = autor

    def actualizar_titulo(self, nuevo_titulo):
        self.titulo = nuevo_titulo

# Crear un objeto
libro1 = Libro("El Principito", "Antoine de Saint-Exupéry")

# Imprimir título inicial
print(f"Título inicial: {libro1.titulo}")

# Actualizar título
libro1.actualizar_titulo("El Principito: Edición Especial")
print(f"Título actualizado: {libro1.titulo}")

~~~

## 5: Clase con métodos personalizados

Crea una clase llamada CuentaBancaria que tenga atributos titular, saldo y métodos para depositar y retirar dinero. Si el saldo no es suficiente, el método retirar debe imprimir un mensaje de error. Crea un objeto, realiza operaciones de depósito y retiro, e imprime el saldo final.

~~~sh
class CuentaBancaria:
    def __init__(self, titular, saldo):
        self.titular = titular
        self.saldo = saldo

    def depositar(self, cantidad):
        self.saldo += cantidad
        print(f"Depositados {cantidad}€. Saldo actual: {self.saldo}€")
    
    def retirar(self, cantidad):
        if cantidad > self.saldo:
            print(f"No hay suficiente saldo para retirar {cantidad}€.")
        else:
            self.saldo -= cantidad
            print(f"Retirados {cantidad}€. Saldo actual: {self.saldo}€")

# Crear un objeto
cuenta = CuentaBancaria("Juan", 100)

# Operaciones
cuenta.depositar(50)
cuenta.retirar(30)
cuenta.retirar(150)

~~~