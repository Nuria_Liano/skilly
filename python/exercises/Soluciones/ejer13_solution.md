# Ejercicios objetos y herencia en python

## Ejercicio 1: Sistema de Usuarios y Administradores

**usuario.py**

~~~py
class Usuario:
    def __init__(self, nombre, email):
        self.nombre = nombre
        self.email = email

    def mostrar_info(self):
        return f"Usuario: {self.nombre}, Email: {self.email}"

class Administrador(Usuario):
    def __init__(self, nombre, email):
        super().__init__(nombre, email)
    
    def eliminar_usuario(self, usuarios, email_a_eliminar):
        usuarios = [u for u in usuarios if u.email != email_a_eliminar]
        return usuarios
~~~

**main.py**

~~~py
from usuario import Usuario, Administrador

# Crear usuarios
usuario1 = Usuario("Ana", "ana@gmail.com")
usuario2 = Usuario("Luis", "luis@gmail.com")
admin = Administrador("Admin", "admin@gmail.com")

# Lista de usuarios
usuarios = [usuario1, usuario2]

# Mostrar información de usuarios
for u in usuarios:
    print(u.mostrar_info())

# Administrador elimina un usuario
usuarios = admin.eliminar_usuario(usuarios, "luis@gmail.com")

print("\nLista actualizada de usuarios:")
for u in usuarios:
    print(u.mostrar_info())

~~~

## Ejercicio 2: Sistema de Animales

**animal.py**

~~~py
class Animal:
    def __init__(self, nombre, especie):
        self.nombre = nombre
        self.especie = especie

    def sonido(self):
        return "Sonido genérico"

class Perro(Animal):
    def __init__(self, nombre):
        super().__init__(nombre, "Perro")

    def sonido(self):
        return "Guau Guau"

class Gato(Animal):
    def __init__(self, nombre):
        super().__init__(nombre, "Gato")

    def sonido(self):
        return "Miau"
~~~

**main.py**

~~~py
from animal import Perro, Gato

# Crear animales
perro = Perro("Rex")
gato = Gato("Misu")

# Mostrar sonidos
animales = [perro, gato]
for animal in animales:
    print(f"{animal.nombre} ({animal.especie}) dice: {animal.sonido()}")
~~~

## Ejercicio 3: Tienda de Productos

**producto.py**

~~~py
class Producto:
    def __init__(self, nombre, precio):
        self.nombre = nombre
        self.precio = precio

    def precio_con_iva(self, tasa_iva):
        return self.precio * (1 + tasa_iva)

    def aplicar_descuento(self, porcentaje_descuento):
        self.precio -= self.precio * (porcentaje_descuento / 100)

    def descripcion(self):
        return f"Producto: {self.nombre}, Precio: {self.precio:.2f}€"

~~~

**electronica.py**

~~~py
from producto import Producto

class Electronica(Producto):
    def __init__(self, nombre, precio, garantia):
        super().__init__(nombre, precio)
        self.garantia = garantia

    def precio_con_iva(self):
        return super().precio_con_iva(0.21)

    def descripcion(self):
        return f"{super().descripcion()}, Garantía: {self.garantia} años"

~~~

**alimentos.py**

~~~py
from producto import Producto

class Alimentos(Producto):
    def __init__(self, nombre, precio, fecha_caducidad):
        super().__init__(nombre, precio)
        self.fecha_caducidad = fecha_caducidad

    def precio_con_iva(self):
        return super().precio_con_iva(0.1)

    def descripcion(self):
        return f"{super().descripcion()}, Fecha de caducidad: {self.fecha_caducidad}"

~~~

**main.py**

~~~py
from electronica import Electronica
from alimentos import Alimentos

# Crear productos
televisor = Electronica("Televisor", 400, 2)
pan = Alimentos("Pan", 2, "2024-12-31")

# Aplicar descuento
televisor.aplicar_descuento(10)

# Mostrar descripción y precios
productos = [televisor, pan]
for producto in productos:
    print(producto.descripcion())
    print(f"Precio con IVA: {producto.precio_con_iva():.2f}€\n")

~~~

## Ejercicio 4: Sistema de Vehículos

**vehiculo.py**

~~~py
class Vehiculo:
    def __init__(self, marca, modelo, velocidad_maxima):
        self.marca = marca
        self.modelo = modelo
        self.velocidad_maxima = velocidad_maxima

    def moverse(self):
        return "El vehículo se está moviendo"

    def tiempo_para_recorrer(self, distancia):
        return distancia / self.velocidad_maxima

~~~

**coche.py**

~~~py
from vehiculo import Vehiculo

class Coche(Vehiculo):
    def __init__(self, marca, modelo, velocidad_maxima, combustible):
        super().__init__(marca, modelo, velocidad_maxima)
        self.combustible = combustible

    def moverse(self):
        return "El coche se mueve por carretera"

    def necesita_mantenimiento(self, kilometros):
        return kilometros > 10000
~~~

**bicicleta.py**

~~~py
from vehiculo import Vehiculo

class Bicicleta(Vehiculo):
    def __init__(self, marca, modelo, velocidad_maxima, tipo):
        super().__init__(marca, modelo, velocidad_maxima)
        self.tipo = tipo

    def moverse(self):
        return "La bicicleta se mueve por caminos"

    def necesita_mantenimiento(self, kilometros):
        return kilometros > 500

~~~

**main.py**

~~~py
from coche import Coche
from bicicleta import Bicicleta

# Crear vehículos
coche = Coche("Toyota", "Corolla", 120, "Gasolina")
bicicleta = Bicicleta("Giant", "Escape 3", 25, "Montaña")

# Mostrar información y calcular tiempo
vehiculos = [coche, bicicleta]
for vehiculo in vehiculos:
    print(f"{vehiculo.marca} {vehiculo.modelo}")
    print(f"Tiempo para recorrer 100 km: {vehiculo.tiempo_para_recorrer(100):.2f} horas")
    print(f"Mantenimiento necesario: {vehiculo.necesita_mantenimiento(600)}")
    print()

~~~

## Ejercicio 5: Sistema de Vehículos

**figura.py**

~~~py
class Figura:
    def area(self):
        pass

    def perimetro(self):
        pass

    def dimensiones_validas(self):
        pass
~~~

**rectangulo.py**

~~~py
from figura import Figura

class Rectangulo(Figura):
    def __init__(self, ancho, alto):
        self.ancho = ancho
        self.alto = alto

    def area(self):
        return self.ancho * self.alto

    def perimetro(self):
        return 2 * (self.ancho + self.alto)

    def dimensiones_validas(self):
        return self.ancho > 0 and self.alto > 0
~~~

**circulo.py**

~~~py
from figura import Figura
import math

class Circulo(Figura):
    def __init__(self, radio):
        self.radio = radio

    def area(self):
        return math.pi * self.radio ** 2

    def perimetro(self):
        return 2 * math.pi * self.radio

    def dimensiones_validas(self):
        return self.radio > 0
~~~

**main.py**

~~~py
from rectangulo import Rectangulo
from circulo import Circulo

# Crear figuras
rectangulo = Rectangulo(4, 5)
circulo = Circulo(3)

# Mostrar información
figuras = [rectangulo, circulo]
for figura in figuras:
    if figura.dimensiones_validas():
        print(f"Área: {figura.area():.2f}")
        print(f"Perímetro: {figura.perimetro():.2f}")
    else:
        print("Dimensiones no válidas")
    print()
~~~