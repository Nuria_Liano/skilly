# Practicando funciones

## 1. Calcular el Área de un Círculo

### Métodos necesarios

- calcular_area_circulo(radio)
  - Parámetros: radio (float) - El radio del círculo.
  - Devuelve: El área del círculo (float).
  - Descripción: Calcula y devuelve el área del círculo usando la fórmula `π * radio^2`
- main()
  - Solicita al usuario el radio del círculo, llama a la función calcular_area_circulo y muestra el resultado.

## 2. Convertir Grados Celsius a Fahrenheit

### Métodos necesarios

- celsius_a_fahrenheit(celsius)
  - Parámetros: celsius (float) - La temperatura en grados Celsius.
  - Devuelve: La temperatura en grados Fahrenheit (float).
  - Descripción: Convierte y devuelve la temperatura en Fahrenheit usando la fórmula `(celsius * 9/5) + 32`.
- main()
  - Solicita al usuario la temperatura en grados Celsius, llama a la función celsius_a_fahrenheit y muestra el resultado.

## 3. Contar Vocales en una Cadena

### Métodos necesarios

- contar_vocales(cadena)
  - Parámetros: cadena (str) - La cadena de texto.
  - Devuelve: El número de vocales en la cadena (int).
  - Descripción: Cuenta y devuelve el número de vocales (a, e, i, o, u) en la cadena.
- main()
  - Solicita al usuario una cadena de texto, llama a la función contar_vocales y muestra el resultado.

## 4. Calcular el Factorial de un Número

### Métodos necesarios

- calcular_factorial(n)
  - Parámetros: n (int) - El número entero.
  - Devuelve: El factorial de `n` (int).
  - Descripción: Calcula y devuelve el factorial de `n` usando un bucle.
- main()
  - Solicita al usuario un número entero, llama a la función calcular_factorial y muestra el resultado.

## 5. Verificar si un Número es Primo

### Métodos necesarios

- es_primo(n)
  - Parámetros: n (int) - El número entero.
  - Devuelve: `True` si el número es primo, `False` en caso contrario (bool).
  - Descripción: Verifica y devuelve si `n` es un número primo.
- main()
  - Solicita al usuario un número entero, llama a la función es_primo y muestra el resultado.

## 6. Invertir una Cadena de Texto

### Métodos necesarios

- invertir_cadena(cadena)
  - Parámetros: cadena (str) - La cadena de texto.
  - Devuelve: La cadena invertida (str).
  - Descripción: Invierte y devuelve la cadena de texto.
- main()
  - Solicita al usuario una cadena de texto, llama a la función invertir_cadena y muestra el resultado.

## 7. Encontrar el Máximo en una Lista de Números

### Métodos necesarios

- encontrar_maximo(lista)
  - Parámetros: lista (list) - La lista de números.
  - Devuelve: El número máximo en la lista (float).
  - Descripción: Encuentra y devuelve el número máximo en la lista.
- main()
  - Solicita al usuario una lista de números (separados por espacios), llama a la función encontrar_maximo y muestra el resultado.

## 8. Calcular la Media de una Lista de Números

### Métodos necesarios

- calcular_media(lista)
  - Parámetros: lista (list) - La lista de números.
  - Devuelve: La media de los números en la lista (float).
  - Descripción: Calcula y devuelve la media de los números en la lista.
- main()
  - Solicita al usuario una lista de números (separados por espacios), llama a la función calcular_media y muestra el resultado.

## 9. Verificar Palíndromo

### Métodos necesarios

- es_palindromo(cadena)
  - Parámetros: cadena (str) - La cadena de texto.
  - Devuelve: `True` si la cadena es un palíndromo, `False` en caso contrario (bool).
  - Descripción: Verifica y devuelve si la cadena es un palíndromo (se lee igual de izquierda a derecha que de derecha a izquierda).
- main()
  - Solicita al usuario una cadena de texto, llama a la función es_palindromo y muestra el resultado.

## 10. Contar Elementos en una Lista

### Métodos necesarios

- contar_elementos(lista)
  - Parámetros: lista (list) - La lista de elementos.
  - Devuelve: El número de elementos en la lista (int).
  - Descripción: Cuenta y devuelve el número de elementos en la lista.
- main()
  - Solicita al usuario una lista de elementos (separados por espacios), llama a la función contar_elementos y muestra el resultado.
