# Bingo

## El programa debe:

- Generar un tablero de Bingo para el jugador como una matriz de 5x5.
- Rellenar el tablero con números aleatorios únicos entre 1 y 75.
- Marcar los números que se llaman (simulados aleatoriamente).
- Verificar si el jugador ha completado una fila, columna o diagonal (Bingo).
- Mostrar el tablero después de cada número llamado.

## Funciones a implementar:

- generar_tablero(): Genera y retorna un tablero de Bingo.
- imprimir_tablero(tablero): Muestra el tablero en la consola.
- marcar_numero(tablero, numero): Marca un número en el tablero si está presente.
- verificar_bingo(tablero): Verifica si el jugador ha hecho Bingo.

~~~py

def imprimir_tablero(tablero):
    for fila in tablero:
        fila_formato = "" #inicializa la fila como cadena vacia
        for celda in fila:
            fila_formato = f"{str(celda):^3} | "
        print(fila_formato[:-3]) #imrpime la fila eliminando el utlimo separador
    print() #salto de linea


~~~