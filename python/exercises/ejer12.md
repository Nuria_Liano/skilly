# Ejercicios de objetos simples

## 1: Crear y acceder a atributos de un objeto

Crea una clase llamada Persona que tenga los atributos nombre, edad y ciudad. Luego, crea un objeto de esta clase con los valores "Ana", 30, "Madrid", e imprime sus atributos en pantalla.

## 2: Método en una clase

Crea una clase llamada Coche con los atributos marca, modelo y color. Define un método llamado descripcion que devuelva una cadena con la descripción del coche en el formato: "Marca: <marca>, Modelo: <modelo>, Color: <color>". Crea un objeto de la clase e imprime su descripción

## 3: Contador de objetos

Crea una clase llamada Producto con un atributo de clase llamado contador que se incremente cada vez que se cree un nuevo objeto de la clase. El constructor debe recibir un nombre para el producto y almacenarlo en un atributo de instancia. Imprime el número de productos creados después de crear tres objetos.

## 4: Modificar atributos de un objeto

Crea una clase llamada Libro con los atributos titulo y autor. Añade un método actualizar_titulo que permita cambiar el título del libro. Crea un objeto, modifica su título e imprime los cambios.

## 5: Clase con métodos personalizados

Crea una clase llamada CuentaBancaria que tenga atributos titular, saldo y métodos para depositar y retirar dinero. Si el saldo no es suficiente, el método retirar debe imprimir un mensaje de error. Crea un objeto, realiza operaciones de depósito y retiro, e imprime el saldo final.