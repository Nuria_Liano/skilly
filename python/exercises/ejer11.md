# Funciones, tuplas y listas

## 1. Sumar Todos los Elementos de una Tupla

### Métodos necesarios

- `sumar_tupla(tupla)`
  - Parámetros: `tupla` (tuple) - La tupla de números.
  - Devuelve: La suma de los elementos en la tupla (int o float).
  - Descripción: Suma todos los elementos de la tupla y devuelve el resultado.
- `main()`
  - Solicita al usuario una tupla de números (separados por comas), llama a la función `sumar_tupla` y muestra el resultado.

## 2. Filtrar Números Pares en una Lista

### Métodos necesarios

- `filtrar_pares(lista)`
  - Parámetros: `lista` (list) - La lista de números.
  - Devuelve: Una lista con los números pares (list).
  - Descripción: Filtra los números pares en la lista y devuelve una nueva lista con esos valores.
- `main()`
  - Solicita al usuario una lista de números (separados por espacios), llama a la función `filtrar_pares` y muestra el resultado.

## 3. Convertir Lista de Tuplas a Diccionario

### Métodos necesarios

- `lista_tuplas_a_diccionario(lista_tuplas)`
  - Parámetros: `lista_tuplas` (list) - Una lista de tuplas, donde cada tupla tiene dos elementos.
  - Devuelve: Un diccionario (dict).
  - Descripción: Convierte una lista de tuplas en un diccionario, usando el primer valor de cada tupla como clave y el segundo como valor.
- `main()`
  - Solicita al usuario una lista de tuplas, llama a la función `lista_tuplas_a_diccionario` y muestra el resultado.

## 4. Verificar si un Elemento Está en una Tupla

### Métodos necesarios

- `elemento_en_tupla(tupla, elemento)`
  - Parámetros: `tupla` (tuple), `elemento` (cualquier tipo de dato).
  - Devuelve: `True` si el elemento está en la tupla, `False` en caso contrario (bool).
  - Descripción: Verifica si un elemento está presente en la tupla.
- `main()`
  - Solicita al usuario una tupla y un elemento, llama a la función `elemento_en_tupla` y muestra el resultado.

## 5. Obtener el Índice de un Elemento en una Lista

### Métodos necesarios

- `obtener_indice(lista, elemento)`
  - Parámetros: `lista` (list), `elemento` (cualquier tipo de dato).
  - Devuelve: El índice del elemento en la lista (int), o -1 si no se encuentra.
  - Descripción: Devuelve el índice de un elemento en la lista. Si el elemento no se encuentra, devuelve -1.
- `main()`
  - Solicita al usuario una lista de elementos y un elemento a buscar, llama a la función `obtener_indice` y muestra el resultado.

## 6. Ordenar una Lista de Tuplas por el Segundo Elemento

### Métodos necesarios

- `ordenar_por_segundo(lista_tuplas)`
  - Parámetros: `lista_tuplas` (list) - Una lista de tuplas donde cada tupla tiene dos elementos.
  - Devuelve: La lista ordenada por el segundo elemento de cada tupla (list).
  - Descripción: Ordena y devuelve la lista de tuplas en función del segundo elemento de cada tupla.
- `main()`
  - Solicita al usuario una lista de tuplas, llama a la función `ordenar_por_segundo` y muestra el resultado.

## 7. Eliminar Elementos Duplicados de una Lista

### Métodos necesarios

- `eliminar_duplicados(lista)`
  - Parámetros: `lista` (list) - Una lista de elementos.
  - Devuelve: Una lista sin elementos duplicados (list).
  - Descripción: Elimina los elementos duplicados y devuelve la lista resultante.
- `main()`
  - Solicita al usuario una lista de elementos (separados por espacios), llama a la función `eliminar_duplicados` y muestra el resultado.

## 8. Contar Ocurrencias de Elementos en una Lista de Tuplas

### Métodos necesarios

- `contar_ocurrencias(lista_tuplas, elemento)`
  - Parámetros: `lista_tuplas` (list) - Una lista de tuplas.
  - Devuelve: El número de ocurrencias del elemento (int).
  - Descripción: Cuenta cuántas veces aparece un elemento en la lista de tuplas y devuelve el número de ocurrencias.
- `main()`
  - Solicita al usuario una lista de tuplas y un elemento a buscar, llama a la función `contar_ocurrencias` y muestra el resultado.

## 9. Obtener el Promedio de Números en una Lista de Tuplas

### Métodos necesarios

- `promedio_tuplas(lista_tuplas)`
  - Parámetros: `lista_tuplas` (list) - Una lista de tuplas donde cada tupla tiene dos números.
  - Devuelve: El promedio de los segundos valores en las tuplas (float).
  - Descripción: Calcula el promedio de los segundos elementos de las tuplas en la lista.
- `main()`
  - Solicita al usuario una lista de tuplas, llama a la función `promedio_tuplas` y muestra el resultado.

## 10. Concatenar Listas de Tuplas

### Métodos necesarios

- `concatenar_listas(lista1, lista2)`
  - Parámetros: `lista1` (list), `lista2` (list) - Dos listas de tuplas.
  - Devuelve: Una lista concatenada (list).
  - Descripción: Concatenar dos listas de tuplas y devolver la lista resultante.
- `main()`
  - Solicita al usuario dos listas de tuplas, llama a la función `concatenar_listas` y muestra el resultado.
