# Ejercicios objetos y herencia en python

## Ejercicio 1: Sistema de Usuarios y Administradores

Define dos clases: Usuario y Administrador. Ambas deben compartir atributos como nombre y email. Administrador debe heredar de Usuario y añadir un método para eliminar usuarios de una lista.

Estructura del proyecto:
- usuario.py
- main.py

## Ejercicio 2: Sistema de Animales

Crea una clase base Animal con atributos nombre y especie, y un método para emitir un sonido genérico. Crea dos clases derivadas, Perro y Gato, que sobrescriban el método de sonido.

Estructura del proyecto:
- animal.py
- main.py

## Ejercicio 3: Tienda de Productos

Crea un sistema para gestionar productos en una tienda. Define una clase base Producto que tenga atributos nombre y precio. También debe incluir un método para calcular el precio con IVA, aplicar descuentos y mostrar una descripción detallada del producto.
Crea las clases derivadas Electronica y Alimentos:

- Electronica debe añadir un atributo de garantía (en años) y calcular el precio con un IVA del 21%.
- Alimentos debe añadir un atributo de fecha de caducidad y calcular el precio con un IVA del 10%.

El programa debe:

- Crear varios productos de ambas categorías.
- Aplicar un descuento al producto de electrónica.
- Mostrar la descripción de cada producto y su precio final con IVA.

## Ejercicio 4: Sistema de Vehículos

Crea un sistema para gestionar vehículos. Define una clase base Vehiculo que tenga atributos como marca, modelo y velocidad_maxima, además de un método para calcular el tiempo necesario para recorrer una distancia dada.

Crea las clases derivadas Coche y Bicicleta:

- Coche debe incluir un atributo adicional para el tipo de combustible (por ejemplo, gasolina o diésel) y un método que indique si necesita mantenimiento (cada 10,000 km).
- Bicicleta debe incluir un atributo para el tipo de bicicleta (por ejemplo, montaña o carretera) y un método que indique si necesita mantenimiento (cada 500 km).

El programa debe:

- Crear un coche y una bicicleta con valores personalizados.
- Mostrar cómo se mueven ambos vehículos.
- Calcular el tiempo necesario para que cada vehículo recorra 100 km.
- Comprobar si necesitan mantenimiento después de un cierto número de kilómetros.

## Ejercicio 5: Sistema de Vehículos

Crea un sistema para trabajar con figuras geométricas. Define una clase base Figura que sirva como plantilla y tenga métodos para calcular el área, el perímetro y verificar si las dimensiones son válidas.

Crea las clases derivadas Rectangulo y Circulo:

- Rectangulo debe incluir atributos para el ancho y el alto, y proporcionar fórmulas para calcular el área y el perímetro. También debe validar que el ancho y el alto sean mayores que cero.
- Circulo debe incluir un atributo para el radio, y proporcionar fórmulas para calcular el área y el perímetro. También debe validar que el radio sea mayor que cero.

El programa debe:

- Crear un rectángulo y un círculo con dimensiones personalizadas.
- Validar si las dimensiones de cada figura son correctas.
- Calcular el área y el perímetro de cada figura válida.
- Mostrar los resultados en pantalla.