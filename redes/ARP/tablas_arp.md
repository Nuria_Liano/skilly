# Tablas ARP (Address Resolution Protocol)

## 1. ¿Qué es ARP?
El **Address Resolution Protocol (ARP)** es un protocolo de comunicaciones que permite a un dispositivo de una red conocer la dirección MAC (Media Access Control) asociada a una dirección IP. Es esencial para la comunicación en redes IPv4.

## 2. ¿Qué es una Tabla ARP?
La **tabla ARP** es una base de datos que almacena asociaciones entre direcciones IP y direcciones MAC. Se encuentra en cada dispositivo de red y se utiliza para resolver direcciones IP en direcciones MAC cuando es necesario transmitir paquetes dentro de una red local (LAN).

## 3. Funcionamiento de ARP
1. Cuando un dispositivo necesita comunicarse con otro dentro de la misma red, verifica si la dirección IP de destino está en su tabla ARP.
2. Si no encuentra la dirección MAC correspondiente, envía una solicitud ARP en la red preguntando "_¿Quién tiene la IP X.X.X.X?_"
3. El dispositivo con esa IP responde con su dirección MAC.
4. La información se almacena en la tabla ARP para futuras comunicaciones.

### **Ejemplo de Comunicación ARP**

Supongamos que un dispositivo con la IP `192.168.1.10` quiere comunicarse con `192.168.1.20`. El proceso sería:

1. `192.168.1.10` revisa su tabla ARP. Si no encuentra la dirección MAC de `192.168.1.20`, envía una solicitud ARP a la red: ¿Quién tiene 192.168.1.20? Dígame su dirección MAC
2. El dispositivo `192.168.1.20` responde con su dirección MAC, por ejemplo, `00:1A:2B:3C:4D:5E`: Yo tengo 192.168.1.20, mi MAC es 00:1A:2B:3C:4D:5E
3. `192.168.1.10` almacena esta información en su tabla ARP y puede comunicarse con `192.168.1.20`.

## 4. Tipos de Entradas en la Tabla ARP
- **Dinámicas:** Se generan automáticamente cuando un dispositivo responde a una solicitud ARP. Tienen un tiempo de vida determinado.
- **Estáticas:** Son configuradas manualmente por el administrador de la red y no caducan.

### **Ejemplo de una Tabla ARP**
Ejecutando el comando `arp -a` en Windows o `arp -n` en Linux/Mac, se obtiene una salida similar a esta:

| Dirección IP    | Dirección MAC       | Tipo      |
|----------------|--------------------|----------|
| 192.168.1.1   | 00:15:5D:4B:01:02  | Dinámica |
| 192.168.1.10  | 00:1A:2B:3C:4D:5E  | Dinámica |
| 192.168.1.50  | 00:AA:BB:CC:DD:EE  | Estática |


## 5. Comandos para Gestionar la Tabla ARP
- **Visualizar la tabla ARP:**
  - Windows:  
    ```sh
    arp -a
    ```
  - Linux/Mac:  
    ```sh
    arp -n
    ```

- **Eliminar una entrada:**
  - Windows:  
    ```sh
    arp -d IP
    ```
  - Linux/Mac:  
    ```sh
    sudo arp -d IP
    ```

- **Agregar una entrada estática:**
  - Windows:  
    ```sh
    arp -s IP MAC
    ```
  - Linux/Mac:  
    ```sh
    sudo arp -s IP MAC
    ```

## 6. 

## 6. Importancia de ARP en la Seguridad de Redes
ARP es vulnerable a ataques como **ARP Spoofing**, donde un atacante engaña a los dispositivos de la red haciéndolos asociar una dirección MAC falsa con una IP válida. Para mitigar estos riesgos, se pueden implementar medidas como:
- Uso de tablas ARP estáticas en dispositivos críticos.
- Activación de inspección ARP en los switches de red.
- Uso de protocolos de seguridad como **DHCP Snooping** y **Dynamic ARP Inspection (DAI)**.
