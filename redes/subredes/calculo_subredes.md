# Cálculo de subredes y máscaras de ded

## ¿Qué es una subred?

Una subred es una división de una red IP en segmentos más pequeños. Esto permite una mejor administración del espacio de direcciones y optimiza el tráfico dentro de la red.

## Máscara de red

La máscara de red define cuántos bits de una dirección IP están reservados para la red y cuántos para los hosts. Se representa en notación decimal (ejemplo: 255.255.255.0) o en notación CIDR (ejemplo: /24).

### Máscara fija y variable

- **Máscara fija**: Todas las subredes tienen el mismo tamaño.
- **Máscara variable (VLSM - Variable Length Subnet Mask)**: Cada subred puede tener un tamaño diferente, optimizando el uso de direcciones IP.

## Cómo Calcular Subredes (Paso a Paso)

1. Identificar la dirección IP y la máscara de red.

Ejemplo: 192.168.1.0/24

2. Determinar cuántas subredes se necesitan.

Supongamos que queremos dividir en 4 subredes.

3. Calcular los bits necesarios para subredes.

Se necesitan 2 bits adicionales para crear 4 subredes (2^2 = 4).
La nueva máscara será /26 (24 + 2).

4. Determinar el número de hosts por subred.

Fórmula: (2^n) - 2, donde n es el número de bits para hosts.

En /26, hay 6 bits para hosts: (2^6) - 2 = 62 hosts.

5. Dividir las direcciones en subredes.

- Subred 1: 192.168.1.0/26 - Hosts: 192.168.1.1 a 192.168.1.62 - Broadcast: 192.168.1.63
- Subred 2: 192.168.1.64/26 - Hosts: 192.168.1.65 a 192.168.1.126 - Broadcast: 192.168.1.127
- Subred 3: 192.168.1.128/26 - Hosts: 192.168.1.129 a 192.168.1.190 - Broadcast: 192.168.1.191
- Subred 4: 192.168.1.192/26 - Hosts: 192.168.1.193 a 192.168.1.254 - Broadcast: 192.168.1.255