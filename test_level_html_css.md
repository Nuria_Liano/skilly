# Nivelador HTML y CSS

## 1. Estructura Básica de HTML

Crea una estructura básica de una página HTML5 que incluya un encabezado, una sección principal y un pie de página.

## 2. Estilizar un Formulario

Pregunta:
¿Cómo se enlaza una hoja de estilo CSS externa a un documento HTML?

HTML:
Crea un formulario HTML que solicite el nombre y el correo electrónico del usuario.

CSS:
Estiliza el formulario para que sea visualmente atractivo.

## 3. Menú de Navegación

Pregunta:
¿Qué es un sistema de cuadrícula (grid system) en CSS y cómo se usa?

HTML:
Crea un menú de navegación con al menos tres enlaces.

CSS:
Estiliza el menú para que los enlaces estén en línea y cambien de color cuando se pase el cursor sobre ellos.

## 4. Modelo de Caja (Box Model)

Pregunta:
¿Qué es el modelo de caja en CSS y cuáles son sus componentes?

HTML:
Crea un contenedor div con algo de contenido dentro.

CSS:
Aplica estilos al contenedor utilizando el modelo de caja (márgenes, bordes, relleno y contenido).

## 5. Diseño Responsivo

Pregunta:
¿Qué es el diseño responsivo y cómo se puede implementar en CSS?

HTML:
Crea una estructura de página básica con un encabezado, una sección principal y un pie de página.

CSS:
Aplica estilos para que el diseño sea responsivo y se adapte a diferentes tamaños de pantalla.