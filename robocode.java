package informatica;

import robocode.*;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/*MyFirst - a robot by Marina*/
public class MyFirst extends Robot {
    /*
     * run: MyFirst's default behavior
     */
    public void run() {// Initialization of the robot should be put here

        // After trying out your robot, try uncommenting the import at the top,
        // and the next line:

        // setColors(Color.red,Color.blue,Color.green); // body,gun,radar
        /*
         * setColors(Body.blue);
         * setColors(Color.red);
         */

        // Robot main loop
        // while(true) {
        // // Replace the next 4 lines with any behavior you would like
        // ahead(100); //Avanza 100 unidades
        // turnGunRight(90); //Gira el cañon 360 grados
        // back(100); // Retrocede 100 unidades
        // turnGunLeft(360); //Gira 90 grados

        // }

        while (true) {
            double moveDistance = Math.random() * 200 + 50; // va a avanzar entre 50 y 200 unidades
            double turnAngle = Math.random() * 180 - 90; // va a girar entre -90 y 90 grados
            // avanza una cantidad aleatoria
            ahead(moveDistance);
            // gira una cantidad aleatoria
            turnRight(turnAngle);
            // mover el cañon 360 grados para buscar otros robots
            turnGunRight(360);

            // retroceso
            back(moveDistance);
            turnRight(turnAngle);
            turnGunRight(360);

            // secuencia de zizag
            ahead(moveDistance);
            turnRight(turnAngle);
            ahead(moveDistance);
            turnLeft(turnAngle);

            // mantener el cañon buscando enemigos
            turnGunRight(360);

        }
    }

    /*
     * 
     * onScannedRobot: What to do when you SEE another robot
     */
    public void onScannedRobot(ScannedRobotEvent e) {// Replace the next line with any behavior you would like

        double closeRange = 100; // distancia cercana
        double mediumRange = 300; // distancia media

        // ajustar el angulo del cañon hacia el robot que detecte
        double gunTurnAmount = getHeading() - getGunHeading() + e.getBearing();
        turnGunRight(gunTurnAmount); // girar el cañon hacia el robot

        // decide si disparar o escapar
        if (shouldFireOrEscape()) {
            // decide si disparar en funcion de la distancia
            if (e.getDistance() < closeRange) {
                fire(3); // disparar y pierde 3 de energia
            } else if (e.getDistance() < mediumRange) {
                fire(2);
            } else {
                fire(1);
            }
        } else {
            escape();
        }

    }

    public void escape() {
        // retrocer
        back(100);

        // girar el angulo aleatorio y evitar ataques predecibles
        double escapeAngle = Math.random() * 180 - 90;
        turnRight(escapeAngle);

        // avanzar para alejarse
        ahead(150);

    }

    public void onHitByBullet(HitByBulletEvent e) { // Evasion
        // Replace the next line with any behavior you would like

        double bulletBearing = e.getBearing(); // Obtener los grados de un enemigo en relacion a mi robot
        double MyGunHeading = getGunHeading(); // Obtener los grados de mi cañon con respecto a mi robot
        double angulo = MyGunHeading + bulletBearing;

        if (angulo < 5) {
            fire(2);
        } else {
            back(20);
            turnLeft(90);
            ahead(20);
        }

    }

    /**
     * 
     * onHitWall: What to do when you hit a WALL
     */
    public void onHitWall(HitWallEvent e) {
        // Replace the next line with any behavior you would like
        turnLeft(180);
        ahead(100);

        out.println("Ouch, I hit a wall bearing " + e.getBearing() + " degrees.");

    }

    // decidir si dispara o escapa segun la energia que tenga
    public boolean shouldFireOrEscape() {
        double energyThreshold = 30; // umbral de energia para decidir si disparar o no

        if (getEnergy() > energyThreshold) {
            return true;
        } else {
            return false;
        }
    }

  public void onHitRobot(HitRobotEvent e) {
    if(e.isMyFault()){
        //retroce si chocaste contra el enemigo
        fire(3);
    }else{
        //si el enemigo choco contra ti, aleje o gire
        turnRight(90);
        ahead(100);
    }  

}
}